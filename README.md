## Auteurs
- Abdou Mbacké NIANG <abdou-mbacke.niang@imt-atalantique.net>
- Saliou Cheikhou FALL <saliou-cheikhou.fall@imt-atlantique.net>

## Notes concernant ce projet

Afin de faire tourner notre application et de rejouer les différentes types d'attaques expérimentées, merci de vous référer au lien suivant servant de rapport à ce projet : https://hackmd.io/@hackdou/rapport_sec_web_app.

Vous trouverez aussi trouver le rapport du projet dans le répertoire racine de ce projet.

Nous partageons aussi avec vous une machine virtuelle déjà configurée dans laquelle tout notre travail est disponible : https://drive.google.com/drive/folders/1hVVqscP-kXalOMmHxF5OOOOvW5-lSEwE?usp=sharing
