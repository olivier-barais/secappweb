package eu.telecom_bretagne.cabinet_recrutement.data.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Session Bean implementation class OffreemploiDAO
 * @author Guillaume PEILLER & Saliou Cheikhou FALL
 */
@Stateless
@LocalBean
public class OffreemploiDAO 
{
	//-----------------------------------------------------------------------------
	/**
	  * RÃ©fÃ©rence vers le gestionnaire de persistance.
	  */
	@PersistenceContext
	EntityManager entityManager;
	//-----------------------------------------------------------------------------
	/**
	  * Default constructor.
	  */
	public OffreemploiDAO()
	{
	 // TODO Auto-generated constructor stub
	}
	 //----------------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Offreemploi> findAll()
	{
		Query query = entityManager.createQuery("select offre from Offreemploi offre order by offre.id desc");
	    List<Offreemploi> l = query.getResultList(); 
	    
	    return l;
	 }
	//----------------------------------------------------------------------------
	public List<Offreemploi> findOffreByEntreprise(int idEntreprise)
	{
	 Query query = entityManager.createQuery("select offres from Offreemploi offres "
	 		+ "where offres.idEntreprise = :idEnt order by offres.id desc");
	 query.setParameter("idEnt", idEntreprise);
	 List<Offreemploi> l = query.getResultList(); 
	    
	 return l;
	}
	//-----------------------------------------------------------------------------
	public List<Offreemploi> findOffreBySecteurActiviteAndNiveauQualification(int idSecteurActivite, 
			String idNiveauQualification){
		Query query = entityManager.createQuery("select offres from Offreemploi offres join "
		  		+ "offres.secteuractivites secteur where secteur.id = :idSA "
		  		+ "and offres.niveauqualification.intitule = :idNQ "
		  		+ "order by offres.id desc");
		query.setParameter("idSA", idSecteurActivite);
		query.setParameter("idNQ", idNiveauQualification);
		List<Offreemploi> l = query.getResultList();
		  
		return l;
	}


	
	  public Offreemploi findById(Integer id)
	  {
	    return entityManager.find(Offreemploi.class, id);
	  }
	  //----------------------------------------------------------------------------
	
	 public Offreemploi persist(Offreemploi offreemploi)
	 {
		 if (offreemploi == null)
			 return offreemploi;
		 
		 try {
			  entityManager.persist(offreemploi);
			  }catch (Exception e) {
				  System.err.println("Problem when saving ");
				  e.printStackTrace();
				  return offreemploi;
			  }
		  return offreemploi;
	  }
	  //-----------------------------------------------------------------------------  
	  public Offreemploi update(Offreemploi offreemploi)
	  {
		  if (offreemploi == null)
			  return offreemploi;
		  
		  try {
			  return entityManager.merge(offreemploi);
			  }catch (Exception e) {
				  System.err.println("Problem when updating ");
				  e.printStackTrace();
				  return offreemploi;
			 }
	  }
	  //-----------------------------------------------------------------------------  
	  public void remove(Offreemploi offreemploi)
	  {

		  if (offreemploi == null)
			return;
			// Search the employee
		  Offreemploi offreemploitodelete = findById(offreemploi.getId());
			try {
				entityManager.remove(offreemploitodelete);
			}catch (Exception e) {
				  System.err.println("Problem when deleting an entity ");				  
				  e.printStackTrace(); 
				  return;
			  }
	  }
	  //-----------------------------------------------------------------------------  
}
