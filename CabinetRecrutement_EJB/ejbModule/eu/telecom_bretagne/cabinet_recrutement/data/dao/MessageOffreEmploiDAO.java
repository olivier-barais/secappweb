package eu.telecom_bretagne.cabinet_recrutement.data.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import eu.telecom_bretagne.cabinet_recrutement.data.model.Messageoffreemploi;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Session Bean implementation class EntrepriseDAO
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class MessageOffreEmploiDAO
{
  //-----------------------------------------------------------------------------
  /**
   * Référence vers le gestionnaire de persistance.
   */
  @PersistenceContext
  EntityManager entityManager;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public MessageOffreEmploiDAO()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  public Messageoffreemploi findById(Integer id)
  {
    return entityManager.find(Messageoffreemploi.class, id);
  }
  //----------------------------------------------------------------------------
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public List<Messageoffreemploi> findAll()
  {
    Query query = entityManager.createQuery("select messageoffreemploi from Messageoffreemploi messageoffreemploi order by messageoffreemploi.id");
    List l = query.getResultList(); 
    
    return (List<Messageoffreemploi>)l;
  }
  //-----------------------------------------------------------------------------
  public List<Messageoffreemploi> findByOffre(Offreemploi offreEmploi)
  {
    Query query = entityManager.createQuery("select messageoffreemploi from Messageoffreemploi messageoffreemploi where messageoffreemploi.offreemploi = :offreEmploi");
    query.setParameter("offreEmploi", offreEmploi);
    List l = query.getResultList(); 
    
    return (List<Messageoffreemploi>)l;
  }
  //-----------------------------------------------------------------------------
  public Messageoffreemploi persist(Messageoffreemploi messageoffreemploi)
  {
	  if (messageoffreemploi == null)
		  return messageoffreemploi;

	  try {
		  entityManager.persist(messageoffreemploi);
		  }catch (Exception e) {
			  System.err.println("Problem when saving ");
			  e.printStackTrace();
			  return messageoffreemploi;
		  }
	  return messageoffreemploi;
  }
  //-----------------------------------------------------------------------------  
  public Messageoffreemploi update(Messageoffreemploi messageoffreemploi)
  {
	  if (messageoffreemploi == null)
		  return messageoffreemploi;
	  
	  try {
		  return entityManager.merge(messageoffreemploi);
		  }catch (Exception e) {
			  System.err.println("Problem when updating ");
			  e.printStackTrace();
			  return messageoffreemploi;
		 }
  }
  //-----------------------------------------------------------------------------  
  public void remove(Messageoffreemploi messageoffreemploi)
  {

	  if (messageoffreemploi == null)
		return;
		// Search the employee
	  Messageoffreemploi messageoffreemploitodelete = findById(messageoffreemploi.getId());
		try {
			entityManager.remove(messageoffreemploitodelete);
		}catch (Exception e) {
			  System.err.println("Problem when deleting an entity ");				  
			  e.printStackTrace(); 
			  return;
		  }
  }
  //-----------------------------------------------------------------------------  
}