package eu.telecom_bretagne.cabinet_recrutement.data.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Session Bean implementation class CandidatureDAO
 * @author Guillaume PEILLER & Saliou Cheikhou FALL
 */
@Stateless
@LocalBean
public class CandidatureDAO
{
  //-----------------------------------------------------------------------------
  /**
   * RÃ©fÃ©rence vers le gestionnaire de persistance.
   */
  @PersistenceContext
  EntityManager entityManager;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public CandidatureDAO()
  {
    // TODO Auto-generated constructor stub
  }
  //----------------------------------------------------------------------------
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public List<Candidature> findAll()
  {
    Query query = entityManager.createQuery("select candidature from Candidature candidature order by candidature.id");
    List<Candidature> l = query.getResultList(); 
    
    return l;
  }
  //----------------------------------------------------------------------------  
  public Candidature findById(Integer id)
  {
    return entityManager.find(Candidature.class, id);
  }

  //-----------------------------------------------------------------------------
  public List<Candidature> findBySecteurActiviteAndNiveauQualification(int idSecteurActivite,
          String idNiveauQualification){
	  Query query = entityManager.createQuery("select candidature from Candidature candidature join "
	  		+ "candidature.secteuractivites secteur where secteur.id = :idSA "
	  		+ "and candidature.niveauqualification.intitule = :idNQ "
	  		+ "order by candidature.id desc");
	  query.setParameter("idSA", idSecteurActivite);
	  query.setParameter("idNQ", idNiveauQualification);
	  List<Candidature> l = query.getResultList();
	  
	  return l;
  }
  //----------------------------------------------------------------------------
	
 public Candidature persist(Candidature candidature)
 {
	 if (candidature == null)
		 return candidature;
	 
	 try {
		  entityManager.persist(candidature);
		  }catch (Exception e) {
			  System.err.println("Problem when saving ");
			  e.printStackTrace();
			  return candidature;
		  }
	  return candidature;
  }
  //-----------------------------------------------------------------------------  
  public Candidature update(Candidature candidature)
  {
	  if (candidature == null)
		  return candidature;
	  
	  try {
		  return entityManager.merge(candidature);
		  }catch (Exception e) {
			  System.err.println("Problem when updating ");
			  e.printStackTrace();
			  return candidature;
		 }
  }
  //-----------------------------------------------------------------------------  
  public void remove(Candidature candidature)
  {

	  if (candidature == null)
		return;
		// Search the employee
	  Candidature candidaturetodelete = findById(candidature.getId());
		try {
			entityManager.remove(candidaturetodelete);
		}catch (Exception e) {
			  System.err.println("Problem when deleting an entity ");				  
			  e.printStackTrace(); 
			  return;
		  }
  }
  //-----------------------------------------------------------------------------  
  public boolean checkCandidatureById(int id)
  {
	  int idToReturn = 0;
	  Query query = entityManager.createQuery("select c.id from Candidature c where c.id = :id"); 
	  query.setParameter("id", id);
	  
	  List <Integer> identifiants = query.getResultList();
	  for(int i : identifiants) {idToReturn = i;}
	  
	  if (idToReturn == id) {return true;}
	  else return false;
  }
  //-----------------------------------------------------------------------------
  //Version permettant de faire de l'injection SQL
  /*
  public List<Candidature> getCandidatureByName(String name)
  {
	  Query query = entityManager.createQuery("select candidature from Candidature candidature where candidature.nom = '" + name + "'");
	  //query.setParameter("name", name);   

	  List<Candidature> l = query.getResultList();
	  return l;
  }
  */
  //name = abc' or '1'='1 pour les tests
  //-----------------------------------------------------------------------------
  public List<Candidature> getCandidatureByName(String name)
  {
	  Query query = entityManager.createQuery("select candidature from Candidature candidature where candidature.nom = :name");
	  query.setParameter("name", name);   

	  List<Candidature> l = query.getResultList();
	  return l;
  }
}
