package eu.telecom_bretagne.cabinet_recrutement.data.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Messagecandidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Messageoffreemploi;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Session Bean implementation class EntrepriseDAO
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class MessageCandidatureDAO
{
  //-----------------------------------------------------------------------------
  /**
   * Référence vers le gestionnaire de persistance.
   */
  @PersistenceContext
  EntityManager entityManager;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public MessageCandidatureDAO()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  public Messagecandidature findById(Integer id)
  {
    return entityManager.find(Messagecandidature.class, id);
  }
  //----------------------------------------------------------------------------
  public List<Messagecandidature> findByOffre(Offreemploi offreEmploi)
  {
    Query query = entityManager.createQuery("select messagecandidature from Messagecandidature messagecandidature where messagecandidature.offreemploi = :offreEmploi");
    query.setParameter("offreEmploi", offreEmploi);
    List l = query.getResultList(); 
    
    return (List<Messagecandidature>)l;
  }
  //----------------------------------------------------------------------------
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public List<Messagecandidature> findAll()
  {
    Query query = entityManager.createQuery("select messagecandidature from Messagecandidature messagecandidature order by messagecandidature.id");
    List l = query.getResultList(); 
    
    return (List<Messagecandidature>)l;
  }
  //-----------------------------------------------------------------------------
  public Messagecandidature persist(Messagecandidature messagecandidature)
  {
	  if (messagecandidature == null)
		  return messagecandidature;

	  try {
		  entityManager.persist(messagecandidature);
		  }catch (Exception e) {
			  System.err.println("Problem when saving ");
			  e.printStackTrace();
			  return messagecandidature;
		  }
	  return messagecandidature;
  }
  //-----------------------------------------------------------------------------  
  public Messagecandidature update(Messagecandidature messagecandidature)
  {
	  if (messagecandidature == null)
		  return messagecandidature;
	  
	  try {
		  return entityManager.merge(messagecandidature);
		  }catch (Exception e) {
			  System.err.println("Problem when updating ");
			  e.printStackTrace();
			  return messagecandidature;
		 }
  }
  //-----------------------------------------------------------------------------  
  public void remove(Messagecandidature messagecandidature)
  {

	  if (messagecandidature == null)
		return;
		// Search the employee
	  Messagecandidature messagecandidaturetodelete = findById(messagecandidature.getId());
		try {
			entityManager.remove(messagecandidaturetodelete);
		}catch (Exception e) {
			  System.err.println("Problem when deleting an entity ");				  
			  e.printStackTrace(); 
			  return;
		  }
	
  }
  //-----------------------------------------------------------------------------  
}