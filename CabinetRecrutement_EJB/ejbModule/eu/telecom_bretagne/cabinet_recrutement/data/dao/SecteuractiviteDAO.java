package eu.telecom_bretagne.cabinet_recrutement.data.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite;

/**
 * Session Bean implementation class SecteuractiviteDAO
 * @author Guillaume PEILLER & Saliou Cheikhou FALL
 */
@Stateless
@LocalBean
public class SecteuractiviteDAO 
{
	//-----------------------------------------------------------------------------
	/**
	  * RÃ©fÃ©rence vers le gestionnaire de persistance.
	  */
	@PersistenceContext
	EntityManager entityManager;
	//-----------------------------------------------------------------------------
	/**
	  * Default constructor.
	  */
	 public SecteuractiviteDAO()
	 {
	   // TODO Auto-generated constructor stub
	 }
	 //----------------------------------------------------------------------------
	 @SuppressWarnings({ "rawtypes", "unchecked" })
	 public List<Secteuractivite> findAll()
	 {
		 Query query = entityManager.createQuery("select secteur from Secteuractivite secteur order by secteur.id");
		 List<Secteuractivite> l = query.getResultList();
		 return l;
	  }
	 //----------------------------------------------------------------------------
	 public Secteuractivite findSecteurById(int id) 
	 {
		 return entityManager.find(Secteuractivite.class, id);
	 }
	 //----------------------------------------------------------------------------
	  public Secteuractivite persist(Secteuractivite secteur)
	  {
		  if (secteur == null)
			  return secteur;

		  try {
			  entityManager.persist(secteur);
			  }catch (Exception e) {
				  System.err.println("Problem when saving ");
				  e.printStackTrace();
				  return secteur;
			  }
		  return secteur;
	  }
	  //-----------------------------------------------------------------------------  
	  public Secteuractivite update(Secteuractivite secteur)
	  {
		  if (secteur == null)
			  return secteur;
		  
		  try {
			  return entityManager.merge(secteur);
			  }catch (Exception e) {
				  System.err.println("Problem when updating ");
				  e.printStackTrace();
				  return secteur;
			 }
	  }
	  //----------------------------------------------------------------------------- 
}
