package eu.telecom_bretagne.cabinet_recrutement.data.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;


/**
 * Session Bean implementation class OffreemploiDAO
 * @author Guillaume PEILLER & Saliou Cheikhou FALL
 */
@Stateless
@LocalBean

public class NiveauqualificationDAO 
{
	//-----------------------------------------------------------------------------
	/**
	  * RÃ©fÃ©rence vers le gestionnaire de persistance.
	  */
	@PersistenceContext
	EntityManager entityManager;
	//-----------------------------------------------------------------------------
	/**
	  * Default constructor.
	  */
	public NiveauqualificationDAO()
	{
	    // TODO Auto-generated constructor stub
	}
	//-----------------------------------------------------------------------------
	public Niveauqualification findByIntitule(String intitule)
	{
		return entityManager.find(Niveauqualification.class, intitule);
	}
	//----------------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Niveauqualification> findAll()
	{
		Query query = entityManager.createQuery("select niveauQualif from Niveauqualification niveauQualif order by niveauQualif.id");
		List l = query.getResultList(); 
	    
	    return (List<Niveauqualification>)l;
	 }
	 //-----------------------------------------------------------------------------
	 public Niveauqualification persist(Niveauqualification niveauQualif)
	 {
		 if (niveauQualif == null)
			 return niveauQualif;
		 
		 try {
			  entityManager.persist(niveauQualif);
			  }catch (Exception e) {
				  System.err.println("Problem when saving ");
				  e.printStackTrace();
				  return niveauQualif;
			  }
		  return niveauQualif;
	  }
	  //-----------------------------------------------------------------------------  
	  public Niveauqualification update(Niveauqualification niveauQualif)
	  {
		  if (niveauQualif == null)
			  return niveauQualif;
		  
		  try {
			  return entityManager.merge(niveauQualif);
			  }catch (Exception e) {
				  System.err.println("Problem when updating ");
				  e.printStackTrace();
				  return niveauQualif;
			 }
	  }
	  //----------------------------------------------------------------------------- 
}
