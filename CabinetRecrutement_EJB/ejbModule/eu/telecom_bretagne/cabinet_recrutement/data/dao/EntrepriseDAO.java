package eu.telecom_bretagne.cabinet_recrutement.data.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise;

/**
 * Session Bean implementation class EntrepriseDAO
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class EntrepriseDAO
{
  //-----------------------------------------------------------------------------
  /**
   * Référence vers le gestionnaire de persistance.
   */
  @PersistenceContext
  EntityManager entityManager;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public EntrepriseDAO()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  public Entreprise findById(Integer id)
  {
    return entityManager.find(Entreprise.class, id);
  }
  //----------------------------------------------------------------------------
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public List<Entreprise> findAll()
  {
    Query query = entityManager.createQuery("select entreprise from Entreprise entreprise order by entreprise.id desc");
    List l = query.getResultList(); 
    
    return (List<Entreprise>)l;
  }
  //-----------------------------------------------------------------------------
  public Entreprise persist(Entreprise entreprise)
  {
	  if (entreprise == null)
		  return entreprise;

	  try {
		  entityManager.persist(entreprise);
		  }catch (Exception e) {
			  System.err.println("Problem when saving ");
			  e.printStackTrace();
			  return entreprise;
		  }
	  return entreprise;
  }
  //-----------------------------------------------------------------------------  
  public Entreprise update(Entreprise entreprise)
  {
	  if (entreprise == null)
		  return entreprise;
	  
	  try {
		  return entityManager.merge(entreprise);
		  }catch (Exception e) {
			  System.err.println("Problem when updating ");
			  e.printStackTrace();
			  return entreprise;
		 }
  }
  //-----------------------------------------------------------------------------  
  public void remove(Entreprise entreprise)
  {

	  if (entreprise == null)
		return;
		// Search the employee
		Entreprise entreprisetodelete = findById(entreprise.getId());
		try {
			entityManager.remove(entreprisetodelete);
		}catch (Exception e) {
			  System.err.println("Problem when deleting an entity ");				  
			  e.printStackTrace(); 
			  return;
		  }
	  
  }
  //-----------------------------------------------------------------------------
  
  public String findEntrepriseNameById(int id)
  {
	  String nameToReturn = null;
	  Query query = entityManager.createQuery("select e.nom from Entreprise e where e.id = :id");
	  query.setParameter("id", id);
	  List <String> name = query.getResultList();
	  
	  for(String s : name) { nameToReturn = s;}
	  
	  return nameToReturn;
  }
  //-----------------------------------------------------------------------------
  public String findEntrepriseDescriptionById(int id)
  {
	  String nameToReturn = null;
	  Query query = entityManager.createQuery("select e.descriptif from Entreprise e where e.id = :id");
	  query.setParameter("id", id);
	  List <String> name = query.getResultList();
	  
	  for(String s : name) { nameToReturn = s;}
	  
	  return nameToReturn;
  }
  //-----------------------------------------------------------------------------
  public boolean checkEntrepriseById(int id)
  {
	  int idToReturn = 0;
	  Query query = entityManager.createQuery("select e.id from Entreprise e where e.id = :id"); 
	  query.setParameter("id", id);
	  
	  List <Integer> identifiants = query.getResultList();
	  for(int i : identifiants) {idToReturn = i;}
	  
	  if (idToReturn == id) {return true;}
	  else return false;
  }
}
