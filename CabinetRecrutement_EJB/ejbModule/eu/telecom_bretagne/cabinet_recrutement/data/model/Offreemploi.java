package eu.telecom_bretagne.cabinet_recrutement.data.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.FetchType.EAGER;


/**
 * The persistent class for the offreemploi database table.
 * 
 */
@Entity
@NamedQuery(name="Offreemploi.findAll", query="SELECT o FROM Offreemploi o")
public class Offreemploi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="OFFREEMPLOI_ID_GENERATOR", sequenceName="OFFREEMPLOI_ID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OFFREEMPLOI_ID_GENERATOR")
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date datedepot;

	private String descriptifmission;

	@Column(name="id_entreprise")
	private Integer idEntreprise;

	private String profilrecherche;

	private String titre;

	//bi-directional many-to-one association to Messagecandidature
	@OneToMany(mappedBy="offreemploi")
	private Set<Messagecandidature> messagecandidatures;

	//bi-directional many-to-one association to Messageoffreemploi
	@OneToMany(mappedBy="offreemploi")
	private Set<Messageoffreemploi> messageoffreemplois;

	//bi-directional many-to-one association to Niveauqualification
	@ManyToOne
	@JoinColumn(name="id_niveauqualification")
	private Niveauqualification niveauqualification;

	//bi-directional many-to-many association to Secteuractivite
	@ManyToMany(mappedBy="offreemplois", fetch = EAGER)
	private Set<Secteuractivite> secteuractivites;

	public Offreemploi() {
	}
	
	public Offreemploi(int idEntreprise, Niveauqualification niveau, String descriptifMission, 
			String profilRecherche, String titreOffre, Date dateDepot) {
		this.idEntreprise = idEntreprise;
		this.niveauqualification = niveau;
		this.descriptifmission = descriptifMission;
		this.profilrecherche = profilRecherche;
		this.titre = titreOffre;
		this.datedepot = dateDepot;
		
		secteuractivites = new HashSet<Secteuractivite>();
	}
	
	public Offreemploi(int idOffre, int idEntreprise, Niveauqualification niveau, String descriptifMission, 
			String profilRecherche, String titreOffre, Date dateDepot) {
		this.id = idOffre;
		this.idEntreprise = idEntreprise;
		this.niveauqualification = niveau;
		this.descriptifmission = descriptifMission;
		this.profilrecherche = profilRecherche;
		this.titre = titreOffre;
		this.datedepot = dateDepot;
		
		secteuractivites = new HashSet<Secteuractivite>();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDatedepot() {
		return this.datedepot;
	}

	public void setDatedepot(Date datedepot) {
		this.datedepot = datedepot;
	}

	public String getDescriptifmission() {
		return this.descriptifmission;
	}

	public void setDescriptifmission(String descriptifmission) {
		this.descriptifmission = descriptifmission;
	}

	public Integer getIdEntreprise() {
		return this.idEntreprise;
	}

	public void setIdEntreprise(Integer idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public String getProfilrecherche() {
		return this.profilrecherche;
	}

	public void setProfilrecherche(String profilrecherche) {
		this.profilrecherche = profilrecherche;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Set<Messagecandidature> getMessagecandidatures() {
		return this.messagecandidatures;
	}

	public void setMessagecandidatures(Set<Messagecandidature> messagecandidatures) {
		this.messagecandidatures = messagecandidatures;
	}

	public Messagecandidature addMessagecandidature(Messagecandidature messagecandidature) {
		getMessagecandidatures().add(messagecandidature);
		messagecandidature.setOffreemploi(this);

		return messagecandidature;
	}

	public Messagecandidature removeMessagecandidature(Messagecandidature messagecandidature) {
		getMessagecandidatures().remove(messagecandidature);
		messagecandidature.setOffreemploi(null);

		return messagecandidature;
	}

	public Set<Messageoffreemploi> getMessageoffreemplois() {
		return this.messageoffreemplois;
	}

	public void setMessageoffreemplois(Set<Messageoffreemploi> messageoffreemplois) {
		this.messageoffreemplois = messageoffreemplois;
	}

	public Messageoffreemploi addMessageoffreemploi(Messageoffreemploi messageoffreemploi) {
		getMessageoffreemplois().add(messageoffreemploi);
		messageoffreemploi.setOffreemploi(this);

		return messageoffreemploi;
	}

	public Messageoffreemploi removeMessageoffreemploi(Messageoffreemploi messageoffreemploi) {
		getMessageoffreemplois().remove(messageoffreemploi);
		messageoffreemploi.setOffreemploi(null);

		return messageoffreemploi;
	}

	public Niveauqualification getNiveauqualification() {
		return this.niveauqualification;
	}

	public void setNiveauqualification(Niveauqualification niveauqualification) {
		this.niveauqualification = niveauqualification;
	}

	public Set<Secteuractivite> getSecteuractivites() {
		return this.secteuractivites;
	}

	public void setSecteuractivites(Set<Secteuractivite> secteuractivites) {
		this.secteuractivites = secteuractivites;
	}

}