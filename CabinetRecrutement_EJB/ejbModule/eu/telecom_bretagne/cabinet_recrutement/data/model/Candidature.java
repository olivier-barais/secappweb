package eu.telecom_bretagne.cabinet_recrutement.data.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.FetchType.EAGER;


/**
 * The persistent class for the candidature database table.
 * 
 */
@Entity
@NamedQuery(name="Candidature.findAll", query="SELECT c FROM Candidature c")
public class Candidature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CANDIDATURE_ID_GENERATOR", sequenceName="CANDIDATURE_ID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CANDIDATURE_ID_GENERATOR")
	private Integer id;

	private String adresseemail;

	private String adressepostale;

	private String cv;

	@Temporal(TemporalType.DATE)
	private Date datedepot;

	@Temporal(TemporalType.DATE)
	private Date datenaissance;

	private String nom;

	private String prenom;

	//bi-directional many-to-one association to Niveauqualification
	@ManyToOne
	@JoinColumn(name="id_niveauqualification")
	private Niveauqualification niveauqualification;

	//bi-directional many-to-one association to Messagecandidature
	@OneToMany(mappedBy="candidature")
	private Set<Messagecandidature> messagecandidatures;

	//bi-directional many-to-one association to Messageoffreemploi
	@OneToMany(mappedBy="candidature")
	private Set<Messageoffreemploi> messageoffreemplois;

	//bi-directional many-to-many association to Secteuractivite
	@ManyToMany(mappedBy="candidatures", fetch = EAGER)
	private Set<Secteuractivite> secteuractivites;

	public Candidature() {
	}
	
	public Candidature(String nom, String prenom, Date dateNaissance, String adressePostale, 
			  String adresseEmail, String cv, Niveauqualification niveau, Date dateDepot) {
		this.nom = nom;
		this.prenom = prenom;
		this.datenaissance = dateNaissance;
		this.adressepostale = adressePostale;
		this.adresseemail = adresseEmail;
		this.cv = cv;
		this.niveauqualification = niveau;
		this.datedepot = dateDepot;
		
		secteuractivites = new HashSet<Secteuractivite>();
	}
	
	public Candidature(int identifiant, String nom, String prenom, Date dateNaissance, String adressePostale, 
			  String adresseEmail, String cv, Niveauqualification niveau, Date dateDepot) {
		this.id = identifiant;
		this.nom = nom;
		this.prenom = prenom;
		this.datenaissance = dateNaissance;
		this.adressepostale = adressePostale;
		this.adresseemail = adresseEmail;
		this.cv = cv;
		this.niveauqualification = niveau;
		this.datedepot = dateDepot;
		
		secteuractivites = new HashSet<Secteuractivite>();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdresseemail() {
		return this.adresseemail;
	}

	public void setAdresseemail(String adresseemail) {
		this.adresseemail = adresseemail;
	}

	public String getAdressepostale() {
		return this.adressepostale;
	}

	public void setAdressepostale(String adressepostale) {
		this.adressepostale = adressepostale;
	}

	public String getCv() {
		return this.cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public Date getDatedepot() {
		return this.datedepot;
	}

	public void setDatedepot(Date datedepot) {
		this.datedepot = datedepot;
	}

	public Date getDatenaissance() {
		return this.datenaissance;
	}

	public void setDatenaissance(Date datenaissance) {
		this.datenaissance = datenaissance;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Niveauqualification getNiveauqualification() {
		return this.niveauqualification;
	}

	public void setNiveauqualification(Niveauqualification niveauqualification) {
		this.niveauqualification = niveauqualification;
	}

	public Set<Messagecandidature> getMessagecandidatures() {
		return this.messagecandidatures;
	}

	public void setMessagecandidatures(Set<Messagecandidature> messagecandidatures) {
		this.messagecandidatures = messagecandidatures;
	}

	public Messagecandidature addMessagecandidature(Messagecandidature messagecandidature) {
		getMessagecandidatures().add(messagecandidature);
		messagecandidature.setCandidature(this);

		return messagecandidature;
	}

	public Messagecandidature removeMessagecandidature(Messagecandidature messagecandidature) {
		getMessagecandidatures().remove(messagecandidature);
		messagecandidature.setCandidature(null);

		return messagecandidature;
	}

	public Set<Messageoffreemploi> getMessageoffreemplois() {
		return this.messageoffreemplois;
	}

	public void setMessageoffreemplois(Set<Messageoffreemploi> messageoffreemplois) {
		this.messageoffreemplois = messageoffreemplois;
	}

	public Messageoffreemploi addMessageoffreemploi(Messageoffreemploi messageoffreemploi) {
		getMessageoffreemplois().add(messageoffreemploi);
		messageoffreemploi.setCandidature(this);

		return messageoffreemploi;
	}

	public Messageoffreemploi removeMessageoffreemploi(Messageoffreemploi messageoffreemploi) {
		getMessageoffreemplois().remove(messageoffreemploi);
		messageoffreemploi.setCandidature(null);

		return messageoffreemploi;
	}

	public Set<Secteuractivite> getSecteuractivites() {
		return this.secteuractivites;
	}

	public void setSecteuractivites(Set<Secteuractivite> secteuractivites) {
		this.secteuractivites = secteuractivites;
	}

}