package eu.telecom_bretagne.cabinet_recrutement.data.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the messageoffreemploi database table.
 * 
 */
@Entity
@NamedQuery(name="Messageoffreemploi.findAll", query="SELECT m FROM Messageoffreemploi m")
public class Messageoffreemploi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MESSAGEOFFREEMPLOI_ID_GENERATOR", sequenceName="MESSAGEOFFREEMPLOI_ID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MESSAGEOFFREEMPLOI_ID_GENERATOR")
	private Integer id;

	private String corpsmessage;

	@Temporal(TemporalType.DATE)
	private Date dateenvoi;

	//bi-directional many-to-one association to Candidature
	@ManyToOne
	@JoinColumn(name="id_candidature")
	private Candidature candidature;

	//bi-directional many-to-one association to Offreemploi
	@ManyToOne
	@JoinColumn(name="id_offreemploi")
	private Offreemploi offreemploi;

	public Messageoffreemploi() {
	}
	
	public Messageoffreemploi(Offreemploi offreEmploi, Candidature candidature, Date dateEnvoi, String corpsMessage) {
		this.offreemploi = offreEmploi;
		this.candidature = candidature;
		this.dateenvoi = dateEnvoi;
		this.corpsmessage = corpsMessage;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCorpsmessage() {
		return this.corpsmessage;
	}

	public void setCorpsmessage(String corpsmessage) {
		this.corpsmessage = corpsmessage;
	}

	public Date getDateenvoi() {
		return this.dateenvoi;
	}

	public void setDateenvoi(Date dateenvoi) {
		this.dateenvoi = dateenvoi;
	}

	public Candidature getCandidature() {
		return this.candidature;
	}

	public void setCandidature(Candidature candidature) {
		this.candidature = candidature;
	}

	public Offreemploi getOffreemploi() {
		return this.offreemploi;
	}

	public void setOffreemploi(Offreemploi offreemploi) {
		this.offreemploi = offreemploi;
	}

}