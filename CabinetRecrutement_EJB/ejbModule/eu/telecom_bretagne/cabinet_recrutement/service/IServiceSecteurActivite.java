package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.List;

import javax.ejb.Remote;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite;

/**
 * Interface du service gérant les offres d'emplois.
 * @author Guillaume & Saliou
 */
@Remote
public interface IServiceSecteurActivite
{
  //-----------------------------------------------------------------------------
  /**
   * Obtention d'une <{@link Entreprise}.
   * 
   * @param id id du secteur d'activité à récupérer.
   * @return
   */
  public Secteuractivite findSecteurByValues(int values);
  //-----------------------------------------------------------------------------
  public void updateSecteurActivite(Secteuractivite secteur);
  //-----------------------------------------------------------------------------
}
