package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.List;

import javax.ejb.Remote;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;

/**
 * Interface du service gérant les offres d'emplois.
 * @author Guillaume & Saliou
 */
@Remote
public interface IServiceNiveauQualification
{
  //-----------------------------------------------------------------------------
  /**
   * Obtention d'une <{@link Niveau Qualification}.
   * 
   * @param id id du niveau de qualification à récupérer.
   * @return
   */
  public Niveauqualification findNiveauQualificationByIntitule(String intitule);
  //-----------------------------------------------------------------------------
}
