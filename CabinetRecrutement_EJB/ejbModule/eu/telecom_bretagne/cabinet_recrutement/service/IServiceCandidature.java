package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;

/**
 * Interface du service gérant les entreprises.
 * @author Philippe TANGUY
 */
@Remote
public interface IServiceCandidature
{
  //-----------------------------------------------------------------------------
  /**
   * Obtention d'une <{@link Entreprise}.
   * 
   * @param id id de l'entreprise à récupérer.
   * @return
   */
  public Candidature getCandidature(int id);
  /**
   * Obtention de la liste de toutes les entreprises.
   * 
   * @return la liste des entreprises dans une {@code List<Entreprise>}.
   */
  public List<Candidature> listeDesCandidatures();
  //-----------------------------------------------------------------------------
  public boolean checkExistenceCandidature(int id);
  //-----------------------------------------------------------------------------
  public List<Candidature> checkExistenceCandidatureName(String name);
  //-----------------------------------------------------------------------------
  public Candidature nouvelleCandidature(String nom, String prenom, Date dateNaissance, String adressePostale, 
		  String adresseEmail, String cv, String[] idsecteur, Niveauqualification niveau, Date dateDepot);
  //-----------------------------------------------------------------------------
  public Candidature updateCandidature(String identifiant, String nom, String prenom, Date dateNaissance, String adressePostale, 
		  String adresseEmail, String cv, String[] idsecteur, Niveauqualification niveau, Date dateDepot, List<Integer> temp);
  //-----------------------------------------------------------------------------
  public void supprimerCandidature(Candidature candidature);
  //-----------------------------------------------------------------------------
}
