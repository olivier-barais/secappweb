package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Interface du service gérant les offres d'emplois.
 * @author Guillaume & Saliou
 */
@Remote
public interface IServiceOffreEmploi
{
  //-----------------------------------------------------------------------------
  /**
   * Obtention d'une <{@link Entreprise}.
   * 
   * @param id id de l'entreprise à récupérer.
   * @return
   */
  public Offreemploi getOffreemploi(int id);
  /**
   * Obtention de la liste de toutes les entreprises.
   * 
   * @return la liste des entreprises dans une {@code List<Entreprise>}.
   */
  public List<Offreemploi> listeDesOffreemploi();
  //-----------------------------------------------------------------------------
  public List<Offreemploi> listeDesOffreemploiEntreprise(int idEntreprise);
  //-----------------------------------------------------------------------------
  public List<Offreemploi> listeDesOffreemploiCandidature(int idSA, String intituleNQ);
  //-----------------------------------------------------------------------------
  public Offreemploi nouvelleOffreEmploi(int idEntreprise, String titreOffre, String descriptifMission, 
		  String profilRecherche, Niveauqualification niveau, String[] idsecteur, Date dateDepot);
  //-----------------------------------------------------------------------------
  public Offreemploi updateOffreEmploi(String idOffre, int idEntreprise, String titreOffre, String descriptifMission, 
		  String profilRecherche, Niveauqualification niveau, String[] idsecteur, Date dateDepot, List<Integer> temp);
  //-----------------------------------------------------------------------------
  public void supprimerOffreEmploi(Offreemploi offreEmploi);
  //-----------------------------------------------------------------------------
  public List<Candidature> findCandidaturesPotentielleBySecteurActivite (int secteuractiviteId, String niveauQualif);
  //-----------------------------------------------------------------------------
}
