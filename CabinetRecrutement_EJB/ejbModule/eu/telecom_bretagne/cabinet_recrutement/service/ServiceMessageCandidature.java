package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.CandidatureDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.MessageCandidatureDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.OffreemploiDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Messagecandidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Session Bean implementation class ServiceEntreprise
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class ServiceMessageCandidature implements IServiceMessageCandidature
{
  //-----------------------------------------------------------------------------
  @EJB private MessageCandidatureDAO         messageCandidatureDAO;
  @EJB private OffreemploiDAO         offreemploiDAO;
  @EJB private CandidatureDAO         candidatureDAO;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ServiceMessageCandidature()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  @Override
  public Messagecandidature getMessageCandidature(int id)
  {
    return messageCandidatureDAO.findById(id);
  }
  //-----------------------------------------------------------------------------
  @Override
  /*public List<Messageoffreemploi> listeDesMessagesEnvoyes()
  {
    return messageOffreEmploiDAO.findAll();
  }*/
  //-----------------------------------------------------------------------------
  public Messagecandidature nouveauMessageCandidature(String idOffre, String idCandidature, Date dateEnvoi, String corpsMessage) {
	  
	  Offreemploi offreEmploi = offreemploiDAO.findById(Integer.parseInt(idOffre));
	  Candidature candidature = candidatureDAO.findById(Integer.parseInt(idCandidature));
	  Messagecandidature nouveauMessage = new Messagecandidature (offreEmploi, candidature, dateEnvoi, corpsMessage);
	  
	  return messageCandidatureDAO.persist(nouveauMessage);
  }
  //-----------------------------------------------------------------------------
  public List<Messagecandidature> findMessageCandidatureByOffre(Offreemploi offreEmploi) {
	  return messageCandidatureDAO.findByOffre(offreEmploi);
  }
  //-----------------------------------------------------------------------------
}