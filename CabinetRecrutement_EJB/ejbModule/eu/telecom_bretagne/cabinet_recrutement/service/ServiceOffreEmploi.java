package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.CandidatureDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.OffreemploiDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.SecteuractiviteDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite;

/**
 * Session Bean implementation class ServiceEntreprise
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class ServiceOffreEmploi implements IServiceOffreEmploi
{
  //-----------------------------------------------------------------------------
  @EJB private OffreemploiDAO         offreemploiDAO;
  @EJB private SecteuractiviteDAO         secteuractiviteDAO;
  @EJB private CandidatureDAO         candidatureDAO;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ServiceOffreEmploi()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  @Override
  public Offreemploi getOffreemploi(int id)
  {
    return offreemploiDAO.findById(id);
  }
  //-----------------------------------------------------------------------------
  @Override
  public List<Offreemploi> listeDesOffreemploi()
  {
    return offreemploiDAO.findAll();
  }
  //-----------------------------------------------------------------------------
  public List<Offreemploi> listeDesOffreemploiEntreprise(int idEntreprise)
  {
    return offreemploiDAO.findOffreByEntreprise(idEntreprise);
  }
  //-----------------------------------------------------------------------------
  public List<Offreemploi> listeDesOffreemploiCandidature(int idSA, String intituleNQ)
  {
	return offreemploiDAO.findOffreBySecteurActiviteAndNiveauQualification(idSA, intituleNQ);
  }
  //-----------------------------------------------------------------------------
  public Offreemploi nouvelleOffreEmploi(int idEntreprise, String titreOffre, String descriptifMission, 
		  String profilRecherche, Niveauqualification niveau, String[] idsecteur, Date dateDepot) {
	  
	  Offreemploi offre = new Offreemploi(idEntreprise, niveau, descriptifMission, profilRecherche, titreOffre, dateDepot);
	  offreemploiDAO.persist(offre);
	  
	  for (String values : idsecteur){
		  Secteuractivite secteur = new Secteuractivite();
	  		
	  	  secteur = secteuractiviteDAO.findSecteurById(Integer.parseInt(values));
	  	  secteur.getOffreemplois().add(offre);
	  	  secteuractiviteDAO.update(secteur);
	  		
	  	  offre.getSecteuractivites().add(secteur);
	  	 }
	 
	  offreemploiDAO.update(offre);
	  return offre;
  }
  //-----------------------------------------------------------------------------
  public Offreemploi updateOffreEmploi(String idOffre, int idEntreprise, String titreOffre, String descriptifMission, 
		  String profilRecherche, Niveauqualification niveau, String[] idsecteur, Date dateDepot, List<Integer> temp) 
  {
	  Offreemploi offreASupprimer = offreemploiDAO.findById(Integer.parseInt(idOffre));
	  for (int i : temp) {
			Secteuractivite secteurASupprimer = new Secteuractivite();
			secteurASupprimer = secteuractiviteDAO.findSecteurById(i);
			
			secteurASupprimer.getOffreemplois().remove(offreASupprimer);
		  	secteuractiviteDAO.update(secteurASupprimer);
		  	offreASupprimer.getSecteuractivites().remove(secteurASupprimer);
		  }
	  offreemploiDAO.update(offreASupprimer);
	  
	  Offreemploi offre = new Offreemploi(Integer.parseInt(idOffre), idEntreprise, niveau, descriptifMission, profilRecherche, titreOffre, dateDepot);
	  offreemploiDAO.update(offre);
	  
	  for (String values : idsecteur) {
		  Secteuractivite secteur = new Secteuractivite();
	  	  secteur = secteuractiviteDAO.findSecteurById(Integer.parseInt(values));
	  	  
	  	  secteur.getOffreemplois().add(offre);
	  	  secteuractiviteDAO.update(secteur);
	  	offre.getSecteuractivites().add(secteur);
	  }
	  offreemploiDAO.update(offre);
	  return offre;
  }
  
  public List<Candidature> findCandidaturesPotentielleBySecteurActivite (int secteuractiviteId, String niveauQualif) {
	  
	 List<Candidature> candidatures = new LinkedList<Candidature>();
	  for ( Candidature c : candidatureDAO.findBySecteurActiviteAndNiveauQualification(secteuractiviteId, niveauQualif)) {
		  candidatures.add(c);
	  }
	  
	  return candidatures;
  }
  //-----------------------------------------------------------------------------
  public void supprimerOffreEmploi(Offreemploi offreEmploi) {
	  offreemploiDAO.remove(offreEmploi);
  }
  //-----------------------------------------------------------------------------
}
