package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Messagecandidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Messageoffreemploi;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Interface du service gérant les messages pour un offre d'emploi.
 * @author Philippe TANGUY
 */
@Remote
public interface IServiceMessageCandidature
{
  //-----------------------------------------------------------------------------
  /**
   * Obtention d'une <{@link MessageCandidature}.
   * 
   * @param id id de Messageoffreemploi à récupérer.
   * @return
   */
  public Messagecandidature getMessageCandidature(int id);
  /**
   * Obtention de la liste de toutes les messages envoyés pour une offre donnée.
   * 
   * @return la liste des entreprises dans une {@code List<Entreprise>}.
   */
  /*public List<Messageoffreemploi> listeDesMessagesEnvoyes();*/
  //-----------------------------------------------------------------------------
  public Messagecandidature nouveauMessageCandidature(String idOffre, String idCandidature, Date dateEnvoi, String corpsMessage);
  //-----------------------------------------------------------------------------
  public List<Messagecandidature> findMessageCandidatureByOffre(Offreemploi offreEmploi);
  //-----------------------------------------------------------------------------
}