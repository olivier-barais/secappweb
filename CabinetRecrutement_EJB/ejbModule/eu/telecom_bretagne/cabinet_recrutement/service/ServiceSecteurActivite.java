package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.SecteuractiviteDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite;

/**
 * Session Bean implementation class ServiceEntreprise
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class ServiceSecteurActivite implements IServiceSecteurActivite
{
  //-----------------------------------------------------------------------------
  @EJB private SecteuractiviteDAO         secteuractiviteDAO;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ServiceSecteurActivite()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  public Secteuractivite findSecteurByValues(int values) 
  {
	  return secteuractiviteDAO.findSecteurById(values);
  }
  //-----------------------------------------------------------------------------
  public void updateSecteurActivite(Secteuractivite secteur)
  {
	  secteuractiviteDAO.update(secteur);
  }
  //-----------------------------------------------------------------------------
}
