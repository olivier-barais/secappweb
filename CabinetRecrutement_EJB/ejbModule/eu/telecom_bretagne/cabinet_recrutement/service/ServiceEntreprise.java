package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.EntrepriseDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise;

/**
 * Session Bean implementation class ServiceEntreprise
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class ServiceEntreprise implements IServiceEntreprise
{
  //-----------------------------------------------------------------------------
  @EJB private EntrepriseDAO         entrepriseDAO;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ServiceEntreprise()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  @Override
  public Entreprise getEntreprise(int id)
  {
    return entrepriseDAO.findById(id);
  }
  //-----------------------------------------------------------------------------
  @Override
  public List<Entreprise> listeDesEntreprises()
  {
    return entrepriseDAO.findAll();
  }
  //-----------------------------------------------------------------------------
  public Entreprise nouvelleEntreprise(String nom, String descriptif, String adressePostale)
  {
	Entreprise entreprise = new Entreprise(nom, descriptif, adressePostale);
	return entrepriseDAO.persist(entreprise);  
  }
  public String nomEntreprise(int id)
  {
	  return entrepriseDAO.findEntrepriseNameById(id);
  }
  //-----------------------------------------------------------------------------
  public String descriptifEntreprise(int id)
  {
	  return entrepriseDAO.findEntrepriseDescriptionById(id);
  }
  //-----------------------------------------------------------------------------
  public boolean checkExistenceEntreprise(int id) 
  {
	 return entrepriseDAO.checkEntrepriseById(id); 
  }
  //-----------------------------------------------------------------------------
  public Entreprise updateEntreprise(Entreprise entreprise)
  {
	  return entrepriseDAO.update(entreprise);
  }
  //-----------------------------------------------------------------------------
  public void supprimerEntreprise(Entreprise entreprise) {
	  entrepriseDAO.remove(entreprise);
  }
  //-----------------------------------------------------------------------------
}
