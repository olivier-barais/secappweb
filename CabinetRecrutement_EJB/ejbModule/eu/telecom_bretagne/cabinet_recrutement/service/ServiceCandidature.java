package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.CandidatureDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.SecteuractiviteDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite;

/**
 * Session Bean implementation class ServiceEntreprise
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class ServiceCandidature implements IServiceCandidature
{
  //-----------------------------------------------------------------------------
  @EJB private CandidatureDAO         candidatureDAO;
  @EJB private SecteuractiviteDAO         secteuractiviteDAO;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ServiceCandidature()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  @Override
  public Candidature getCandidature(int id)
  {
    return candidatureDAO.findById(id);
  }
  //-----------------------------------------------------------------------------
  @Override
  public List<Candidature> listeDesCandidatures()
  {
    return candidatureDAO.findAll();
  }
  //-----------------------------------------------------------------------------
  public boolean checkExistenceCandidature(int id)
  {
	return candidatureDAO.checkCandidatureById(id);
  }
  //-----------------------------------------------------------------------------
  public Candidature nouvelleCandidature(String nom, String prenom, Date dateNaissance, String adressePostale, 
		  String adresseEmail, String cv, String[] idsecteur, Niveauqualification niveau, Date dateDepot) 
  {
	  Candidature candidature = new Candidature(nom, prenom, dateNaissance, adressePostale, adresseEmail, cv, niveau, dateDepot);
	  candidatureDAO.persist(candidature);
	  
	  for (String values : idsecteur){
		  Secteuractivite secteur = new Secteuractivite();
	  		
	  	  secteur = secteuractiviteDAO.findSecteurById(Integer.parseInt(values));
	  	  secteur.getCandidatures().add(candidature);
	  	  secteuractiviteDAO.update(secteur);
	  		
	  	  candidature.getSecteuractivites().add(secteur);
	  	 }
	 
	 candidatureDAO.update(candidature);
	 return candidature;
  }
  //-----------------------------------------------------------------------------
  public Candidature updateCandidature(String identifiant, String nom, String prenom, Date dateNaissance, String adressePostale, 
		  String adresseEmail, String cv, String[] idsecteur, Niveauqualification niveau, Date dateDepot, List<Integer> temp)
  {
	  Candidature candidatureASupprimer = candidatureDAO.findById(Integer.parseInt(identifiant));
	  for (int i : temp) {
			Secteuractivite secteurASupprimer = new Secteuractivite();
			secteurASupprimer = secteuractiviteDAO.findSecteurById(i);
			
			secteurASupprimer.getCandidatures().remove(candidatureASupprimer);
		  	secteuractiviteDAO.update(secteurASupprimer);
		  	candidatureASupprimer.getSecteuractivites().remove(secteurASupprimer);
		  }
		    candidatureDAO.update(candidatureASupprimer);
	  
	  Candidature candidature = new Candidature(Integer.parseInt(identifiant), nom, prenom, dateNaissance, adressePostale, adresseEmail, cv, niveau, dateDepot);
	  candidatureDAO.update(candidature);
	  
	  for (String values : idsecteur) {
		  Secteuractivite secteur = new Secteuractivite();
	  	  secteur = secteuractiviteDAO.findSecteurById(Integer.parseInt(values));
	  	  
	  	  secteur.getCandidatures().add(candidature);
	  	  secteuractiviteDAO.update(secteur);
	  	  candidature.getSecteuractivites().add(secteur);
	  }
	  candidatureDAO.update(candidature);
	  return candidature;
  }
  //-----------------------------------------------------------------------------
  public void supprimerCandidature(Candidature candidature) {
	  candidatureDAO.remove(candidature);
  }
  //-----------------------------------------------------------------------------
  public List<Candidature> checkExistenceCandidatureName(String name) {
	  return candidatureDAO.getCandidatureByName(name);
  }
  //-----------------------------------------------------------------------------
}
