package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.NiveauqualificationDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;

/**
 * Session Bean implementation class ServiceNiveauQualification
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class ServiceNiveauQualification implements IServiceNiveauQualification
{
  //-----------------------------------------------------------------------------
  @EJB private NiveauqualificationDAO         niveauqualificationDAO;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ServiceNiveauQualification()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  public Niveauqualification findNiveauQualificationByIntitule(String intitule)
  {
	  return niveauqualificationDAO.findByIntitule(intitule);
  }
  //-----------------------------------------------------------------------------
}
