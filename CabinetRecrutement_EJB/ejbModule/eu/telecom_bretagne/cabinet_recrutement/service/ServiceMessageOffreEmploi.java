package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.CandidatureDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.MessageOffreEmploiDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.OffreemploiDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Messageoffreemploi;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Session Bean implementation class ServiceEntreprise
 * @author Philippe TANGUY
 */
@Stateless
@LocalBean
public class ServiceMessageOffreEmploi implements IServiceMessageOffreEmploi
{
  //-----------------------------------------------------------------------------
  @EJB private MessageOffreEmploiDAO         messageOffreEmploiDAO;
  @EJB private OffreemploiDAO         offreemploiDAO;
  @EJB private CandidatureDAO         candidatureDAO;
  //-----------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ServiceMessageOffreEmploi()
  {
    // TODO Auto-generated constructor stub
  }
  //-----------------------------------------------------------------------------
  @Override
  public Messageoffreemploi getMessageOffreEmploi(int id)
  {
    return messageOffreEmploiDAO.findById(id);
  }
  //-----------------------------------------------------------------------------
  @Override
  /*public List<Messageoffreemploi> listeDesMessagesEnvoyes()
  {
    return messageOffreEmploiDAO.findAll();
  }*/
  //-----------------------------------------------------------------------------
  public Messageoffreemploi nouvelleMessageoffreemploi(String idOffre, String idCandidature, Date dateEnvoi, String corpsMessage) {
	  
	  Offreemploi offreEmploi = offreemploiDAO.findById(Integer.parseInt(idOffre));
	  Candidature candidature = candidatureDAO.findById(Integer.parseInt(idCandidature));
	  Messageoffreemploi nouveauMessageOffreEmploi = new Messageoffreemploi (offreEmploi, candidature, dateEnvoi, corpsMessage);
	  
	  return messageOffreEmploiDAO.persist(nouveauMessageOffreEmploi);
  }
  //-----------------------------------------------------------------------------
  public List<Messageoffreemploi> findMessageOffreEmploiByOffre(Offreemploi offreEmploi){
	  return messageOffreEmploiDAO.findByOffre(offreEmploi);
  }
  //-----------------------------------------------------------------------------
}
