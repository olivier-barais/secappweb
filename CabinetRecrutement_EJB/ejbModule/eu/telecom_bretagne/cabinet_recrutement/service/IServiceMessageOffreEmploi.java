package eu.telecom_bretagne.cabinet_recrutement.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.telecom_bretagne.cabinet_recrutement.data.model.Messageoffreemploi;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;

/**
 * Interface du service gérant les messages pour un offre d'emploi.
 * @author Philippe TANGUY
 */
@Remote
public interface IServiceMessageOffreEmploi
{
  //-----------------------------------------------------------------------------
  /**
   * Obtention d'une <{@link Messageoffreemploi}.
   * 
   * @param id id de Messageoffreemploi à récupérer.
   * @return
   */
  public Messageoffreemploi getMessageOffreEmploi(int id);
  /**
   * Obtention de la liste de toutes les messages envoyés pour une offre donnée.
   * 
   * @return la liste des entreprises dans une {@code List<Entreprise>}.
   */
  /*public List<Messageoffreemploi> listeDesMessagesEnvoyes();*/
  //-----------------------------------------------------------------------------
  public Messageoffreemploi nouvelleMessageoffreemploi(String idOffre, String idCandidature, Date dateEnvoi, String corpsMessage);
  //-----------------------------------------------------------------------------
  public List<Messageoffreemploi> findMessageOffreEmploiByOffre(Offreemploi offreEmploi);
  //-----------------------------------------------------------------------------

}
