-- Titre :             Création base cabinet recrutement version élèves.sql
-- Version :           1.0
-- Date création :     28 juin 2011
-- Date modification : 30 août 2011
-- Auteur :            Philippe Tanguy
-- Description :       Script de création de la base de données pour le SI "gestion de cabinet de
--                     recrutement"
--                     Note : script pour PostgreSQL 8.4
--                     Ebauche du script : ne contient pour le moment que la table "entreprise".

-- +----------------------------------------------------------------------------------------------+
-- | Suppression des tables                                                                       |
-- +----------------------------------------------------------------------------------------------+



drop table if exists IndexSecteurActiviteCandidature;
drop table if exists IndexSecteurActiviteOffreEmploi;
drop table if exists MessageOffreEmploi;
drop table if exists MessageCandidature;
drop table if exists Candidature;
drop table if exists OffreEmploi;
drop table if exists SecteurActivite;
drop table if exists NiveauQualification;
drop table if exists Entreprise;
-- +----------------------------------------------------------------------------------------------+
-- | Création des tables                                                                          |
-- +----------------------------------------------------------------------------------------------+

create table Entreprise
(
	id              serial primary key,
	nom             varchar(50) not null,
	descriptif      text,
	adresse_postale varchar(30) -- Pour simplifier, adresse_postale = ville.
);

create table NiveauQualification
(
	intitule            text primary key
);


create table SecteurActivite
(
	id				serial primary key,
	intitule		varchar(50)	
);


create table OffreEmploi
(
	id              			serial primary key,
	id_entreprise 				integer references Entreprise(id),
	id_niveauQualification		text references NiveauQualification(intitule),
	descriptifMission			text,
	profilRecherche				varchar(50) not null,
	titre						varchar(30),
	dateDepot					date
);

create table Candidature
(
	id              			serial primary key,
	id_niveauQualification		text references NiveauQualification(intitule),
	nom							varchar(50),
	prenom						varchar(50),
	dateNaissance				date,
	adressePostale				text,
	adresseEmail				varchar(50),
	CV							text,
	dateDepot					date
);

create table MessageCandidature
(
	id              	serial primary key,
	id_offreEmploi		integer references OffreEmploi(id),
	id_candidature		integer references Candidature(id),
	dateEnvoi			date,	
	corpsMessage		text
);

create table MessageOffreEmploi
(
	id              	serial primary key,
	id_offreEmploi		integer references OffreEmploi(id),
	id_candidature		integer references Candidature(id),
	dateEnvoi			date,
	corpsMessage		text
);


create table IndexSecteurActiviteOffreEmploi
(
	id_offreEmploi		integer references OffreEmploi(id),
	id_secteurActivite	integer references SecteurActivite(id)
);


create table IndexSecteurActiviteCandidature
(
	id_candidature		integer references Candidature(id),
	id_secteurActivite	integer references SecteurActivite(id)
);


-- +----------------------------------------------------------------------------------------------+
-- | Insertion de quelques données de pour les tests                                              |
-- +----------------------------------------------------------------------------------------------+

-- Insertion des secteurs d'activité

insert into Entreprise values (nextval('Entreprise_id_seq'),'Télécom Bretagne','Télécom Bretagne est une grande école pionnière en formation, en recherche et en entrepreneuriat.','Plouzané');
insert into Entreprise values (nextval('Entreprise_id_seq'),'ENIB','Une école d''ingénieur juste à côté...','Plouzané');
insert into Entreprise values (nextval('Entreprise_id_seq'),'Friedel Corp','Notre vision, Conjuguer le numérique et l énergie pour transformer la société et lindustrie par la formation, la recherche et linnovation.','Aulnay-sous-bois');

insert into NiveauQualification values ('CAP/BEP');
insert into NiveauQualification values ('Bac');
insert into NiveauQualification values ('Bac+3');
insert into NiveauQualification values ('Bac+5');
insert into NiveauQualification values ('Doctorat');


insert into OffreEmploi values (nextval('OffreEmploi_id_seq'),3,'Bac+5','CDD jeune ingénieur : ingénierie des modèles, architecture des systèmes.','Compétences et Profil :','CDD jeune ingénieur','07/07/2020');
insert into OffreEmploi values (nextval('OffreEmploi_id_seq'),3,'Bac+3','CDD jeune technicien : technicite des modèles, architecture des systèmes.','Compétences et Profil :','CDD jeune technicien','08/07/2020');

insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Hôtellerie/Restauration/Tourisme');
insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Informatique');
insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Ressources Humaines');
insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Assistanat/Secrétariat');
insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Commercial');
insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Finance/Banque');
insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Télécom/Réseaux');
insert into SecteurActivite values (nextval('SecteurActivite_id_seq'),'Formation/Enseignement');

insert into Candidature values (nextval('Candidature_id_seq'),'Bac+5','Tanguy','Philippe','21/12/1961','BREST','philippe.tanguy@imt-atlantique.fr','Formation=========2002 Diplôme dingénieur du Conservatoire National des Arts et Métiers, spécialité Informatique','07/07/2017');
insert into Candidature values (nextval('Candidature_id_seq'),'Bac+5','TOTO','Robert','11/11/1111','BREST','robert.toto@gmail.com','-Tagada -Tsoin -Tsoi','07/07/2017');
insert into Candidature values (nextval('Candidature_id_seq'),'Bac+3','M OISELLE','Jeanne','11/11/1111','BRUXELLES',' 	moiselle.jeanne@imt-atlantique.fr','Au début petite et laide, elle apparaît uniquement comme prétexte...','07/07/2017');

insert into IndexSecteurActiviteCandidature values (1, 1);
insert into IndexSecteurActiviteCandidature values (1, 2);
insert into IndexSecteurActiviteCandidature values (1, 5); --revoir la cle primaire pour secteurActivite --> intitule
insert into IndexSecteurActiviteCandidature values (3, 2);
insert into IndexSecteurActiviteCandidature values (3, 3);

insert into IndexSecteurActiviteOffreEmploi values (1, 1);
insert into IndexSecteurActiviteOffreEmploi values (1, 2);
insert into IndexSecteurActiviteOffreEmploi values (1, 5); --revoir la cle primaire pour secteurActivite --> intitule

