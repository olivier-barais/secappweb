<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceSecteurActivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceNiveauQualification,
                java.util.Date, java.util.LinkedList, java.util.List"%>
             
<%
    IServiceOffreEmploi serviceOffreEmploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
	IServiceSecteurActivite serviceSecteurActivite = (IServiceSecteurActivite) ServicesLocator.getInstance().getRemoteInterface("ServiceSecteurActivite");
	IServiceNiveauQualification serviceNiveauQualification = (IServiceNiveauQualification) ServicesLocator.getInstance().getRemoteInterface("ServiceNiveauQualification");
	IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
	
	String id = request.getParameter("id_offre");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-users"></i> Mettre � jour les informations de l'offre d'emploi </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
      	
      	<%
      	String idOffre = request.getParameter("id_offre_emploi");
      	String idEnt = request.getParameter("id_entreprise");
      	String titreOffre = request.getParameter("titre");
    	String descriptifMission = request.getParameter("descriptifmission");
    	String profilRecherche = request.getParameter("profilrecherche");
    	String niveauQualif = request.getParameter("niveauQualif");
    	String[] idsecteur = request.getParameterValues("idsecteur");
    	List<Integer> temp = new LinkedList<Integer>();
    	
    	if (titreOffre ==null && niveauQualif==null && idsecteur==null) {
      		try {
      	        
      	        int idOffreToUpdate = Integer.parseInt(id);
      	      	Offreemploi offreEmploi = serviceOffreEmploi.getOffreemploi(idOffreToUpdate);
      	        %>
      	
         <form action="template.jsp" method="get">
         <input type="hidden" name="action" value="maj_offre" />
         <input type="hidden" name="id_offre_emploi" value="<%=offreEmploi.getId()%>">
         <input type="hidden" name="id_entreprise" value="<%=offreEmploi.getIdEntreprise()%>">
         
           <div class="form-group">
			  <input type="text" class="form-control" placeholder="Titre de l'offre*" name="titre" value="<%=offreEmploi.getTitre()%>">
		   </div>
		   <div class="form-group">
			  <textarea class="form-control" placeholder="Description de la mission" name="descriptifmission" rows="4" ><%=offreEmploi.getDescriptifmission()%></textarea><br>
		   </div>
		   <div class="form-group">
			  <textarea class="form-control" placeholder="Profil recherch�" name="profilrecherche" rows="4"><%=offreEmploi.getProfilrecherche()%></textarea><br>
		   </div>
		   
		   <div class="col-lg-3">
			   <div class="form-group">
		           <label>Niveau de qualification*</label>
		           <%
			           		String intitule = offreEmploi.getNiveauqualification().getIntitule();
			       %>
		           <small>
			           <div class="radio">
			           		<label><input type="radio" name="niveauQualif" value="CAP/BEP" <% if(("CAP/BEP").equals(intitule)) {%> checked <%}%> >CAP/BEP</label>
			           </div>
			           <div class="radio">
			           		<label><input type="radio" name="niveauQualif" value="Bac" <% if(("Bac").equals(intitule)) {%> checked <%}%> >Bac</label>
			           </div>
			           <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Bac+3" <% if(("Bac+3").equals(intitule)) {%> checked <%}%> >Bac+3</label>
		           	   </div>
		           	   <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Bac+5" <% if(("Bac+5").equals(intitule)) {%> checked <%}%>>Bac+5</label>
		           	   </div>
		           	   <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Doctorat" <% if(("Doctorat").equals(intitule)) {%> checked <%}%> >Doctorat</label>
		           	   </div>
	           	   </small>
	           </div>
           </div>
           
           <div class="col-lg-9">
	           <div class="form-group">
	           		<label>Secteur(s) d'activit�*</label>
	           		<%
		           			List<Integer> idSecteurActiviteAModifier =new LinkedList<Integer>();
			           		for ( Secteuractivite secteurActivite : offreEmploi.getSecteuractivites()){
			           			idSecteurActiviteAModifier.add(secteurActivite.getId());
		           			}
		           	%>
	           		<small>
	           			<table width="100%" border="0">
	           				<tbody>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="1" <% if(idSecteurActiviteAModifier.contains(1)) {%> checked <%}%>> H�tellerie/Restauration/Tourisme</td>
	           						<td><input type="checkbox" name="idsecteur" value="2" <% if(idSecteurActiviteAModifier.contains(2)) {%> checked <%}%>> Informatique</td>  
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="3" <% if(idSecteurActiviteAModifier.contains(3)) {%> checked <%}%>> Ressources Humaines</td>
	           						<td><input type="checkbox" name="idsecteur" value="4" <% if(idSecteurActiviteAModifier.contains(4)) {%> checked <%}%>> Assistanat/Secr�tariat </td> 
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="5" <% if(idSecteurActiviteAModifier.contains(5)) {%> checked <%}%>> Commercial</td>
	           						<td><input type="checkbox" name="idsecteur" value="6" <% if(idSecteurActiviteAModifier.contains(6)) {%> checked <%}%>> Finance/Banque</td>  
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="7" <% if(idSecteurActiviteAModifier.contains(7)) {%> checked <%}%>> T�l�com/R�seaux</td>
	           						<td><input type="checkbox" name="idsecteur" value="8" <% if(idSecteurActiviteAModifier.contains(8)) {%> checked <%}%>> Formation/Enseignement</td> 
	           						<td>&nbsp;</td>
	           					</tr>
	           				</tbody>
	           			</table>
	           		</small>
	           </div>
           </div>
           
		   <h1>&nbsp;</h1>
		   <h5>&nbsp;</h5>
		   <div class="text-center">
			  <button type="submit" class="btn btn-success">Valider</button>
              <button type="reset" class="btn btn-warning">Annuler</button>
           </div>
           <div>
          		<br>
          		<em>* il est obligatoire de renseigner ces cases.</em>
          </div>
          </form>
          
          <div class="col-lg-12 text-center">
          <br>
           	  <button class="btn btn-danger" data-toggle="modal" data-target="#mymodalSuppressionOffreEmploi">
                <i class="glyphicon glyphicon-remove-sign fa-lg"></i> Supprimer cette offre d'emploi
              </button>
              
              <!-- Modal -->
				<div class="modal fade" id="mymodalSuppressionOffreEmploi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">�tes-vous s�r de vouloir supprimer cette offre d'emploi ?</h4>
				      </div>
				      <div class="modal-body">
				        Cette op�ration est irr�versible... ! 
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				        <button type="button" class="btn btn-danger" onclick="window.location.href='template.jsp?action=suppression_offre_emploi&idOffreASupprimer=<%=offreEmploi.getId()%>'">Supprimer</button>
				      </div>
				    </div>
				  </div>
				</div>
	       </div>
          
          <%}
      		catch (Exception e) {}
      	}
          else {
        	  if (titreOffre.equals("")) {%>
            	<div class="row col-xs-offset-1 col-xs-10">
              	<div class="panel panel-red">
                  	<div class="panel-heading ">Impossible de traiter la demande</div>
                      <div class="panel-body text-center">
                      	<p class="text-danger">Il n'est pas possible de r�f�rencer une offre d'emploi qui ne poss�de pas de titre.</p>
                      </div>
                  </div>
              </div> <!-- /.row col-xs-offset-1 col-xs-10 -->
              <% }
        	  
        	  else {
        		  
        		  int idOffreToUpdate = Integer.parseInt(idEnt);
        	      Offreemploi offreEmploi = serviceOffreEmploi.getOffreemploi(idOffreToUpdate);
    	      	 
    	      	  //Recup�rer les id des secteurs d'activit� � mettre � jour
    	      	  for (Secteuractivite secteurASupprimer : offreEmploi.getSecteuractivites()) {
      	        	  temp.add(secteurASupprimer.getId());
      	          }
    	      	  
    	      	  
    			  //--------------------------------------------------------------------------------------------------
        	  	 Niveauqualification niveau = serviceNiveauQualification.findNiveauQualificationByIntitule(niveauQualif);
        	 	 Date dateDepot = new Date();
        	 	 int idEntreprise = Integer.parseInt(idEnt);
        	 	 
        	 	 Offreemploi offreEmploiUpdated = serviceOffreEmploi.updateOffreEmploi(idOffre, idEntreprise, titreOffre, descriptifMission, profilRecherche, niveau, idsecteur, dateDepot, temp);
        	 	 
          %>
          		<div class="col-lg-offset-2 col-lg-8 col-xs-12">
          			<div class="panel panel-success">
          				<div class="panel-heading">Informations mises � jour pour l'offre d'emploi</div>
          			<div class="panel-body">
			        <div class="table-responsive">
			            <small>
			            <table class="table">
			              <tbody>
			                <tr class="success">
			                  <td width="200"><strong>Identifiant</strong></td>
			                  <td><%=offreEmploiUpdated.getId()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Entreprise</strong></td>
			                  <td><%=serviceEntreprise.nomEntreprise(offreEmploiUpdated.getIdEntreprise())%>
			                  	<br>
			                  	<br>
			                  	  <%=serviceEntreprise.descriptifEntreprise(offreEmploiUpdated.getIdEntreprise())%>
			                  </td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Niveau de qualification</strong></td>
			                  <td><%=offreEmploiUpdated.getNiveauqualification().getIntitule()%></td>
			                </tr>                
			                <tr class="warning">
			                  <td><strong>Description de la mission</strong></td>
			                  <td><%=offreEmploiUpdated.getDescriptifmission()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Profil Recherch�</strong></td>
			                  <td><%=offreEmploiUpdated.getProfilrecherche()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Titre</strong></td>
			                  <td><%=offreEmploiUpdated.getTitre()%></td>
			                </tr>                                
			                <tr class="warning">
			                  <td><strong>Date de depot</strong></td>
			                  <td><%=Utils.date2String(offreEmploiUpdated.getDatedepot())%></td>
			                </tr>
								<tr class="warning">
			                <td><strong>Secteur d'activit�</strong></td>
			                <td>
							<%
								for(Secteuractivite s : offreEmploiUpdated.getSecteuractivites())
								{
							     %>
			                  	   <ul><li><%=s.getIntitule()%></li></ul> <!-- Mettre une balise br au besoin -->
			                	<%
								}
			                %>
			                </td>
			                </tr>
			              </tbody>
			            </table>
			            </small>      
			        </div>
		       	</div>
		       	</div>
		       </div>
				
          <%}
        	  }%>
		</div>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
