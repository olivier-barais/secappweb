<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw""></i> Connexion </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
      	
         <form action="template.jsp?action=connexion_reussie" method="get">
         <input type="hidden" name="action" value="connexion_reussie"/>
           <div class="form-group">
			  <input type="text" class="form-control" placeholder="Identifiant" name="identrepriseorcandidature">
		   </div>
		   
		   <div class="text-center">
			  <button type="submit" class="btn btn-success">Se connecter</button>
              <button type="reset" class="btn btn-danger">Annuler la saisie</button>
           </div>
          </form>
          <p/> <!-- pour mettre une espace -->
          
		  <div class="panel panel-info">
			<div class="panel-heading">Information</div>
			<div class="panel-body">
				L'identifiant est la cl� primaire pr�fix�e de :
     			<ul>
          			<li>pour une entreprise : <code>ENT_</code> <em>(ENT_12 par exemple)</em></li>
          			<li>pour une candidature : <code>CAND_</code> <em>(CAND_7 par exemple)</em></li>
          		</ul>
          		<em>Note : l'identification se fait sans mot de passe.</em>
            </div>
          </div>
                
          
		</div>

      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
