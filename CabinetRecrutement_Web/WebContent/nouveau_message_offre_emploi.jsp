<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
				eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Messageoffreemploi,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceMessageOffreEmploi,
                java.util.List, java.util.Date"%>

<%
  String idOffre = request.getParameter("idOffre");
  String idCandidature = request.getParameter("idCandidat");

  IServiceOffreEmploi serviceOffreemploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
  IServiceCandidature serviceCandidature = (IServiceCandidature) ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");
  IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
  IServiceMessageOffreEmploi serviceMessageOffreEmploi = (IServiceMessageOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceMessageOffreEmploi");
  
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-envelope fa-fw"></i> Messages</h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
      	<%
      		String verificationEnvoi = request.getParameter("envoyer");
	      	String idOffreEmploi = request.getParameter("id_offre");
	      	String idCandidatureAssocie = request.getParameter("id_candidature");
	    	String corpsMessage = request.getParameter("corps_message");
	    	
	    	if (verificationEnvoi == null) {
	    		try {
	    			Offreemploi offreEmploi = serviceOffreemploi.getOffreemploi(Integer.parseInt(idOffre));
	    			  Candidature candidature = serviceCandidature.getCandidature(Integer.parseInt(idCandidature));
      	%>
      	<!-- Table de l'ent�te -->
      		<div class="alert alert-info">
            	<table border="0" align="center">
                  <tr>
                    <td width=200 align="center">
                      <i class="fa fa-th fa-fw"></i><br/><%=serviceEntreprise.nomEntreprise(offreEmploi.getIdEntreprise())%><br/> <em><%=offreEmploi.getTitre()%></em>
                    </td>
                    <td width=50 align="center">
                      <i class="fa fa-arrow-right fa-fw"></i>
                    </td>
                    <td width=200 align="center">
                      <i class="fa fa-user fa-fw"></i><br/><%=candidature.getNom()%>&nbsp; <%=candidature.getPrenom()%><br/>
                      &nbsp;
                    </td>
                  </tr>
                </table>
            </div>
            
        <!-- Formulaire du message -->
            <form role="form" action="template.jsp" method="get">
            
                <input type="hidden" name="action" value="nouveau_message_offre_emploi" />
                <input type="hidden" name="envoyer" value="" />
                <input type="hidden" name="id_offre" value="<%=offreEmploi.getId()%>" />
                <input type="hidden" name="id_candidature" value="<%=candidature.getId()%>" />
                
                <div class="form-group">
                  <textarea class="form-control" placeholder="Contenu du message" rows="5" name="corps_message"></textarea>
                </div>
			   <div class="text-center">
				  <button type="submit" class="btn btn-success">Valider</button>
	              <button type="reset" class="btn btn-warning">Annuler</button>
	           </div>
              </form>
           <%}
	    		catch (Exception e) {}
             }
	    	else 
	    	{
	    		Date dateEnvoi = new Date();
	    		Messageoffreemploi nouveauMessageOffre = serviceMessageOffreEmploi.nouvelleMessageoffreemploi(idOffreEmploi, idCandidatureAssocie, dateEnvoi, corpsMessage);
	    		Candidature candidature = serviceCandidature.getCandidature(Integer.parseInt(idCandidatureAssocie));
    			Offreemploi offreEmploi = serviceOffreemploi.getOffreemploi(Integer.parseInt(idOffreEmploi));
    			Entreprise entreprise = serviceEntreprise.getEntreprise(offreEmploi.getIdEntreprise());
	    	%>
	    	
	    	<div class="panel panel-success">
	            <div class="panel-heading"> Nouveau message envoy�</div>
	            <div class="panel-body">
		            <small>
			            <table class="table">
				            <tbody>
					            <tr class="success">
						            <td><strong>Identifiant</strong></td>
						            <td><%=nouveauMessageOffre.getId()%></td></tr>
					            <tr class="warning">
						            <td><strong>Offre emploi concern�e</strong></td>
						            <td><%=offreEmploi.getTitre()%> (<%=entreprise.getNom()%>)</td>
					            </tr>
					            <tr class="warning">
						            <td><strong>Candidature</strong></td>
						            <td><%=candidature.getNom()%> <%=candidature.getPrenom()%> (CAND_<%=candidature.getId()%>)</td>
					            </tr>
					            <tr class="warning">
						            <td><strong>Date d'envoi</strong></td>
						            <td><%=Utils.date2String(nouveauMessageOffre.getDateenvoi())%></td>
					            </tr>
					            <tr class="warning">
						            <td><strong>Contenu du message</strong></td>
						            <td><%=nouveauMessageOffre.getCorpsmessage()%></td>
					            </tr>
				            </tbody>
			            </table>
		            </small>
	            </div>
            </div>
	    	
	    	<%	
	    	}
            %>
          
        </div>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
