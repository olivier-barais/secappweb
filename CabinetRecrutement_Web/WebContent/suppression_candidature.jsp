<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature"%>
                
<%	
	session.invalidate(); //deconnexion lorsque la candidature est supprim�e
	String identifiant = request.getParameter("id");
	IServiceCandidature serviceCandidature = (IServiceCandidature) ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");

	Candidature candidature = serviceCandidature.getCandidature(Integer.parseInt(identifiant));
%>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw"></i>Supression de la candidature</h3></div> <!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row col-xs-offset-1 col-xs-10">
						<div class="panel panel-red">
							<div class="panel-body text-center">
								<p class="text-danger"><strong>La candidature <%=candidature.getNom()%> (identifiant=CAND_<%=candidature.getId()%>) a �t� supprim�e.</p>
								<%serviceCandidature.supprimerCandidature(candidature);%>
							</div>
						</div>
					</div> <!-- /.row col-xs-offset-1 col-xs-10 --> 
				</div> <!-- /.panel-body -->
		</div> <!-- /.panel -->
	</div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->