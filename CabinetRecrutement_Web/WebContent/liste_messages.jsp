<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
				eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi, 
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceMessageOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceMessageCandidature,                              
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Messageoffreemploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Messagecandidature, 
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi,
                java.util.List, java.util.LinkedList"%>

<%
  String idEntreprise = request.getParameter("id_entreprise");
  String idCandidature = request.getParameter("id_candidature");
  String idSA = request.getParameter("idSA");

  IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
  IServiceOffreEmploi  serviceOffreEmploi  = (IServiceOffreEmploi)  ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
  IServiceCandidature  serviceCandidature  = (IServiceCandidature)  ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");
  IServiceMessageOffreEmploi  serviceMessageOffreEmploi  = (IServiceMessageOffreEmploi)  ServicesLocator.getInstance().getRemoteInterface("ServiceMessageOffreEmploi");
  IServiceMessageCandidature  serviceMessageCandidature  = (IServiceMessageCandidature)  ServicesLocator.getInstance().getRemoteInterface("ServiceMessageCandidature");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-envelope fa-fw"></i> Liste des messages </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="dataTable_wrapper">
        
        
          <!--  Table messages re�us -->
          <div class="panel-heading"><h4><i class="glyphicon glyphicon-import"></i> Messages re�us </h4></div> <!-- /.panel-heading -->
          <table class="table table-striped table-bordered table-hover">
            <!--
              Nom des colonnes
            -->
            <thead>
              <tr>
                <th>Identifiant</th>
                <th>Exp�diteur</th>
                <th>Offre</th>
                <th>Date d'envoi</th>
                <th>Message</th>
              </tr>
            </thead>
            <!--
              Contenu du tableau
            -->
            <tbody>
            
            <%if (idEntreprise != null){
            	
				List<Offreemploi> listeoffre = serviceOffreEmploi.listeDesOffreemploiEntreprise(Integer.parseInt(idEntreprise));
            	
            	List<Messagecandidature> messagesCandidaturesEntreprise = new LinkedList<Messagecandidature>();
            	for (Offreemploi offre:listeoffre){
            		  List<Messagecandidature> messagesCandidaturesRecus = serviceMessageCandidature.findMessageCandidatureByOffre(offre);
            		  
            		  for (Messagecandidature message:messagesCandidaturesRecus){
            			  messagesCandidaturesEntreprise.add(message);
            		  }
            	 }

            	for (Messagecandidature message:messagesCandidaturesEntreprise){
            %>
		            <tr>
			            <td> <%=message.getId() %></td>
			            <td><a href="template.jsp?action=infos_candidature&id=<%=message.getCandidature().getId()%>"><%=message.getCandidature().getPrenom()%> <%=message.getCandidature().getNom()%></td>
			            <td><a href="template.jsp?action=infos_offreemploi&id=<%=message.getOffreemploi().getId()%>"> <%=message.getOffreemploi().getTitre()%></a></td>      
			            <td> <%=Utils.date2String(message.getDateenvoi())%></td>   
			            <td> <%=message.getCorpsmessage()%></td>    
		            </tr>
             
             <% }
            }
            else if (idCandidature != null) {
				Candidature candidature = serviceCandidature.getCandidature(Integer.parseInt(idCandidature));
            	
            	List<Offreemploi> listeoffre = serviceOffreEmploi.listeDesOffreemploiCandidature(Integer.parseInt(idSA), candidature.getNiveauqualification().getIntitule()) ; //a modifier
            	
            	List<Messageoffreemploi> messagesOffresEmploiRecus = new LinkedList<Messageoffreemploi>();
            	for (Offreemploi offre:listeoffre){
            		  List<Messageoffreemploi> messagesOffreEmploi = serviceMessageOffreEmploi.findMessageOffreEmploiByOffre(offre);
            		  
            		  for (Messageoffreemploi message: messagesOffreEmploi){
            			  messagesOffresEmploiRecus.add(message);
            		  }
            	 }

            	for (Messageoffreemploi message:messagesOffresEmploiRecus){
             %>
		            <tr>
			            <td> <%=message.getId() %></td>
			            <td><a href="template.jsp?action=infos_entreprise&id=<%=message.getOffreemploi().getIdEntreprise()%>"><%=serviceEntreprise.getEntreprise(message.getOffreemploi().getIdEntreprise()).getNom()%></td>
			            <td><a href="template.jsp?action=infos_offreemploi&id=<%=message.getOffreemploi().getId()%>"> <%=message.getOffreemploi().getTitre()%></a></td>      
			            <td> <%=Utils.date2String(message.getDateenvoi())%></td>   
			            <td> <%=message.getCorpsmessage()%></td>    
		            </tr>
             <% }
            }%>
            
            </tbody>
            </table>
            </div>
            
         <!--  Table messages envoy�s -->
                  
          <div class="panel-heading"><h4><i class="glyphicon glyphicon-export"></i> Messages envoy�s </h4></div> <!-- /.panel-heading -->
          <table class="table table-striped table-bordered table-hover">
            <!--
              Nom des colonnes
            -->
            <thead>
              <tr>
                <th>Identifiant</th>
                <th>Destinataire</th>
                <th>Offre</th>
                <th>Date d'envoi</th>
                <th>Message</th>
              </tr>
            </thead>
            <!--
              Contenu du tableau
            -->
            <tbody>
            <%
            if (idEntreprise != null){
            	List<Offreemploi> listeoffre = serviceOffreEmploi.listeDesOffreemploiEntreprise(Integer.parseInt(idEntreprise));
            	
            	List<Messageoffreemploi> messagesoffreemploientreprise = new LinkedList<Messageoffreemploi>();
            	for (Offreemploi offre:listeoffre){
            		  List<Messageoffreemploi> messagesoffreemploi = serviceMessageOffreEmploi.findMessageOffreEmploiByOffre(offre);
            		  
            		  for (Messageoffreemploi message:messagesoffreemploi){
            			  messagesoffreemploientreprise.add(message);
            		  }
            	 }

            	for (Messageoffreemploi message:messagesoffreemploientreprise){
            %>
		            <tr>
			            <td> <%=message.getId() %></td>
			            <td><a href="template.jsp?action=infos_candidature&id=<%=message.getCandidature().getId()%>"><%=message.getCandidature().getPrenom()%> <%=message.getCandidature().getNom()%></td>
			            <td><a href="template.jsp?action=infos_offreemploi&id=<%=message.getOffreemploi().getId()%>"> <%=message.getOffreemploi().getTitre()%></a></td>      
			            <td> <%=Utils.date2String(message.getDateenvoi())%></td>   
			            <td> <%=message.getCorpsmessage()%></td>    
		            </tr>
             
             <% }
              }
              
            else if (idCandidature != null) {
            	Candidature candidature = serviceCandidature.getCandidature(Integer.parseInt(idCandidature));
            	
            	List<Offreemploi> listeoffre = serviceOffreEmploi.listeDesOffreemploiCandidature(Integer.parseInt(idSA), candidature.getNiveauqualification().getIntitule()) ; //a modifier
            	
            	List<Messagecandidature> messagesCandidatureCandidature = new LinkedList<Messagecandidature>();
            	for (Offreemploi offre:listeoffre){
            		  List<Messagecandidature> messagesCandidature = serviceMessageCandidature.findMessageCandidatureByOffre(offre);
            		  
            		  for (Messagecandidature message: messagesCandidature){
            			  messagesCandidatureCandidature.add(message);
            		  }
            	 }

            	for (Messagecandidature message:messagesCandidatureCandidature){
             %>
		            <tr>
			            <td> <%=message.getId() %></td>
			            <td><a href="template.jsp?action=infos_entreprise&id=<%=message.getOffreemploi().getIdEntreprise()%>"><%=serviceEntreprise.getEntreprise(message.getOffreemploi().getIdEntreprise()).getNom()%></td>
			            <td><a href="template.jsp?action=infos_offreemploi&id=<%=message.getOffreemploi().getId()%>"> <%=message.getOffreemploi().getTitre()%></a></td>      
			            <td> <%=Utils.date2String(message.getDateenvoi())%></td>   
			            <td> <%=message.getCorpsmessage()%></td>    
		            </tr>
             <% }
              } %>
            
            </tbody>
          </table>
          </div>
        </div> <!-- /.table-responsive -->
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
