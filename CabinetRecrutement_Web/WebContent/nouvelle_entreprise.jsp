<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                java.util.regex.Matcher,
                java.util.regex.Pattern"%>
   
             
<%
    IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
	String nomEntreprise = request.getParameter("nomentreprise");
	String descriptif = request.getParameter("descriptifentreprise");
	String adressePostale = request.getParameter("adressepostale");
	
	
	
	Pattern xss = Pattern.compile("^(<)");	
%>

<%     if ( xss.matcher(nomEntreprise).find() || xss.matcher(descriptif).find() || xss.matcher(adressePostale).find() ) { %>
	<div class="row">
	  <div class="col-lg-12">
	    <div class="panel panel-default">
	      <div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw"></i>Cr�ation Entreprise</h3></div> <!-- /.panel-heading -->
	      <div class="panel-body">
			<div class="row col-xs-offset-1 col-xs-10">
	      	<div class="panel panel-red">
	      		<div class="panel-heading ">Tentative d'une attaque XSS relev�e</div>
	      		<div class="panel-body text-center">
	      			<p class="text-danger"><strong> "In every group of animals living together, there's always an individual that doesn't fit in. These invidividuals naturally become isolated from the group and generally tend to reach the same end."
Nice try (^-^) ! Saliou & Abdou</strong></p>
	      			<button type="button"class="btn btn-danger" onclick="window.location.href='template.jsp?action=formulaire_nouvelle_entreprise'">Retour...</button>
	      		</div>
	      	</div>
	      </div> <!-- /.row col-xs-offset-1 col-xs-10 --> 
	      </div> <!-- /.panel-body -->
	    </div> <!-- /.panel -->
	  </div> <!-- /.col-lg-12 -->
	</div> <!-- /.row -->
	<% }
	
	else {
		Entreprise entreprise = serviceEntreprise.nouvelleEntreprise(nomEntreprise, descriptif, adressePostale);
	%>


<div class="row">
  <div class="col-lg-12">
  
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-th"></i>Nouvelle entreprise cr��e</h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
        
        <div class="col-lg-offset-2 col-lg-8 col-xs-12">
	        <div class="table-responsive">
	            <small>
	            <table class="table">
	              <tbody>
	                <tr class="success">
	                  <td width="200"><strong>Identifiant (login)</strong></td>
	                  <td>ENT_<%=entreprise.getId()%></td>
	                </tr>
	                <tr class="warning">
	                  <td><strong>Nom</strong></td>
	                  <td><%=entreprise.getNom()%></td>
	                </tr>
	                <tr class="warning">
	                  <td><strong>Adresse postale (ville)</strong></td>
	                  <td><%=entreprise.getAdressePostale()%></td>
	                </tr>
	                <tr class="warning">
	                  <td><strong>Descriptif</strong></td>
	                  <td><%=Utils.text2HTML(entreprise.getDescriptif())%></td>
	                </tr>
	              </tbody>
	            </table>
	            </small>      
	        </div>
        </div>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->

  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
<%}%>
