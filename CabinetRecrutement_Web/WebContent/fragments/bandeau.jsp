<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite"%>

<%
  IServiceOffreEmploi serviceOffreEmploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
  Object utilisateur = session.getAttribute("utilisateur");
%>


<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <h1><a class="navbar-brand" href="template.jsp">Cabinet de Jean Pierre Koff <br /> Requiescat in pace </a></h1><br/>
  
  <% if( utilisateur != null ) { 
		if(utilisateur instanceof Entreprise){
			Entreprise e = (Entreprise) utilisateur;
  %>
  		<h4 class="navbar-brand"><i class="fa fa-th fa-fw"></i><%=e.getNom()%>&nbsp;<em>(entreprise)</em></h4>
  <% }
		else if (utilisateur instanceof Candidature) {
			Candidature c = (Candidature) utilisateur;
  %>
  		<h4 class="navbar-brand"><i class="fa fa-users fa-fw"></i><%=c.getNom()%>&nbsp;<em>(candidature)</em></h4>
  <% 
	}
  }
  %>
</div> <!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">
	<!-- /.dropdown -->
		<% if(utilisateur != null) {%>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-envelope fa-fw"></i>
					<i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-messages">
					<li><a href="#">
						<i class="glyphicon glyphicon-import"></i>
							Messages re�us<span class="pull-right text-muted"><em>Nombre</em></span></a>
					</li>
					<li class="divider"></li>
					<li><a href="#">
						<i class="glyphicon glyphicon-export"></i>
							Messages envoy�s<span class="pull-right text-muted"><em>Nombre</em></span></a>
					</li>
					<li class="divider"></li>
					<%if( utilisateur instanceof Entreprise){
						Entreprise e = (Entreprise) utilisateur;%>
    					<li><a class="text-center" href="template.jsp?action=liste_messages&id_entreprise=<%=e.getId()%>"><strong>Lire les messages</strong> <i class="fa fa-angle-right"></i></a></li>
    				<%}
					else if (utilisateur instanceof Candidature) {
						Candidature c = (Candidature) utilisateur;
						
						int idSecteur = 0;
						for(Secteuractivite s : c.getSecteuractivites())
						{
							idSecteur = s.getId();
						}%>
						<li><a class="text-center" href="template.jsp?action=liste_messages&id_candidature=<%=c.getId()%>&idSA=<%=idSecteur%>"><strong>Lire les messages</strong> <i class="fa fa-angle-right"></i></a></li>
					<%} %>
				</ul>
			</li>
	<% } %>

	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
		<i class="fa fa-caret-down"></i></a>
			<ul class="dropdown-menu dropdown-user">
				<% if( utilisateur != null ) { 
					if( utilisateur instanceof Entreprise){
						Entreprise e = (Entreprise) utilisateur;
					
				%>
				<li><a href="#"><i class="fa fa-user fa-fw"></i> <%=e.getNom()%></a></li>
				<% }
					else if (utilisateur instanceof Candidature) {
						Candidature c = (Candidature) utilisateur;
					
				%>
				<li><a href="#"><i class="fa fa-user fa-fw"></i> <%=c.getNom()%></a></li>
				<% 
					} 
				%>
				<li class="divider"></li>
				<li><a href="deconnexion.jsp"><i class="fa fa-sign-out  fa-fw"></i>Se deconnecter</a></li>
				<%
				}
				else{%>
				<li><a href="#"><i class="fa fa-user fa-fw"></i> Aucun utilisateur connect�</a></li>
				<li class="divider"></li>
				<li><a href="template.jsp?action=connexion"><i class="fa fa-sign-in  fa-fw"></i>Se connecter</a></li>
				<li class="divider"></li>
				<li><a href="template.jsp?action=connexion_candidat_par_nom"><i class="fa fa-sign-in  fa-fw"></i>Connexion candidat par nom</a></li>
				<% } %>
				
			</ul> <!-- /.dropdown-user -->
	</li>
	<!-- /.dropdown -->
</ul> <!-- /.navbar-top-links -->