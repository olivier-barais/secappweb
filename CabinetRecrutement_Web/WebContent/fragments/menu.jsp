<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature"%>

<%
  Object utilisateur = session.getAttribute("utilisateur");

  IServiceEntreprise  serviceEntreprise  = (IServiceEntreprise)  ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
  IServiceOffreEmploi  serviceOffreEmploi  = (IServiceOffreEmploi)  ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
  IServiceCandidature  serviceCandidature  = (IServiceCandidature)  ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");
  
%>

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
  
    <ul class="nav" id="side-menu">

      <!--
        MENU PRINCIPAL
      -->

      <li><a href="index.jsp"><i class="fa fa-home"></i> Accueil</a></li>
      <li>
        <a href="#"><i class="fa fa-th"></i> Gestion des entreprises<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li><a href="template.jsp?action=formulaire_nouvelle_entreprise">Nouvelle entreprise</a></li>
          <li><a href="template.jsp?action=liste_entreprises">Liste des entreprises</a></li>
          <li><a href="template.jsp?action=liste_offreemploi">Liste de toutes les offres d'emploi</a></li>
        </ul> <!-- /.nav-second-level -->
      </li>
      <li>
        <a href="#"><i class="fa fa-users"></i> Gestion des candidatures<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li><a href="template.jsp?action=nouvelle_candidature">Nouvelle candidature</a></li>
          <li><a href="template.jsp?action=liste_candidatures">Liste des candidatures</a></li>
        </ul> <!-- /.nav-second-level -->
      </li>

      <!--
        MENU Entreprise
      -->
	
	<% if (utilisateur != null && utilisateur instanceof Entreprise){ 
		Entreprise e = (Entreprise) utilisateur;
		int nbOffres = serviceOffreEmploi.listeDesOffreemploiEntreprise(e.getId()).size();%>
	<li><h4>&nbsp;</h4></li>
      <li>
        <a href="#"><i class="fa fa-th"></i> Menu <strong>Entreprise</strong><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li><a href="template.jsp?action=formulaire_modification_entreprise&id=<%=e.getId()%>">Mettre � jour les informations de l'Entreprise</a></li>
          <li><a href="template.jsp?action=formulaire_ajout_offreemploi&idEntreprise=<%=e.getId()%>">Nouvelle offre d'emploi</a></li>
          <li><a href="template.jsp?action=liste_mes_offreemploi_entreprise&id=<%=e.getId()%>">Liste de mes offres d'emploi (<%=nbOffres%>)</a></li>
        </ul> <!-- /.nav-second-level -->
      </li>
		<% 	} %>


      <!--
        MENU Candidature
      -->
	
	<% if (utilisateur != null && utilisateur instanceof Candidature){ 
		Candidature c = (Candidature) utilisateur;
		int idSecteur = 0;
		int nbOffres = 0;

			for(Secteuractivite s : c.getSecteuractivites())
			{
				idSecteur = s.getId();
				nbOffres = serviceOffreEmploi.listeDesOffreemploiCandidature(idSecteur, c.getNiveauqualification().getIntitule()).size();
			} %>

	  <li><h4>&nbsp;</h4></li>
      <li>
        <a href="#"><i class="fa fa-users"></i> Menu <strong>Candidature</strong><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li><a href="template.jsp?action=formulaire_modification_candidature&id=<%=c.getId()%>">Mettre � jour les informations de la Candidature</a></li>
          <li><a href="template.jsp?action=liste_emploi_potentiel&idSA=<%=idSecteur%>&idNQ=<%=c.getNiveauqualification().getIntitule()%>&idCandidature=<%=c.getId()%>">Liste des offres d'emploi potentielles(<%=nbOffres%>)</a></li>
        </ul> <!-- /.nav-second-level -->
      </li>
		<% } %>



      <!--
        MENU SECONDAIRE
      -->

      <li><h4>&nbsp;</h4></li>
      <li><a href="http://formations.telecom-bretagne.eu/fip_inf210_fil_rouge/" target="_blank"><i class="fa fa-question-circle"></i> Documentation du fil rouge</a></li>
      <li><a href="http://srv-labs-006.enst-bretagne.fr/CabinetRecrutement_Web/" target="_blank"><i class="fa fa-certificate"></i> D�monstration du prototype</a></li>
      <li><a href="bootstrap/pages/" target="_blank"><i class="fa fa-thumbs-up"></i> D�mo du template SB Admin 2</a></li>
    </ul>
  </div> <!-- /.sidebar-collapse -->
</div> <!-- /.navbar-static-side -->
