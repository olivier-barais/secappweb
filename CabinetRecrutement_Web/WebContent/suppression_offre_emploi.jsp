<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi"%>
                
<%	
	String identifiantOffre = request.getParameter("idOffreASupprimer");
	IServiceOffreEmploi serviceOffreEmploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");

	Offreemploi offreEmploi = serviceOffreEmploi.getOffreemploi(Integer.parseInt(identifiantOffre));
%>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw"></i>Supression de l'offre d'emploi</h3></div> <!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row col-xs-offset-1 col-xs-10">
						<div class="panel panel-red">
							<div class="panel-body text-center">
								<p class="text-danger"><strong>L'offre d'emploi <%=offreEmploi.getTitre()%> (identifiant = <%=offreEmploi.getId()%>) a �t� supprim�e.</p>
								<%serviceOffreEmploi.supprimerOffreEmploi(offreEmploi);%>
							</div>
						</div>
					</div> <!-- /.row col-xs-offset-1 col-xs-10 --> 
				</div> <!-- /.panel-body -->
		</div> <!-- /.panel -->
	</div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->