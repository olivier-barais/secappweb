<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise"%>
                
<%	
	session.invalidate(); //deconnexion lorsque l'entreprise est supprim�e
	String identifiant = request.getParameter("id");
	IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");

	Entreprise entreprise = serviceEntreprise.getEntreprise(Integer.parseInt(identifiant));
%>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw"></i>Supression de l'entreprise</h3></div> <!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row col-xs-offset-1 col-xs-10">
						<div class="panel panel-red">
							<div class="panel-body text-center">
								<p class="text-danger"><strong>L'entreprise <%=entreprise.getNom()%> (identifiant=ENT_<%=entreprise.getId()%>) a �t� supprim�e.</p>
								<%serviceEntreprise.supprimerEntreprise(entreprise);%>
							</div>
						</div>
					</div> <!-- /.row col-xs-offset-1 col-xs-10 --> 
				</div> <!-- /.panel-body -->
		</div> <!-- /.panel -->
	</div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->