<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi,
                java.util.Set"%>

<%
IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");

  String erreur = null;
  String idStringValue = request.getParameter("id");
  int id = -1;
  Offreemploi offreemploi = null;
  
  if(idStringValue == null)
  {
    erreur="Aucun identifiant d'offre d'emploi n'est fourni dans la demande.";
  }
  else
  {
    try
    {
      id = new Integer(idStringValue);
      // C'est OK : on a bien un id
      IServiceOffreEmploi serviceOffreEmploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
      offreemploi = serviceOffreEmploi.getOffreemploi(id);
      if(offreemploi == null)
      {
        erreur="Aucune offre d'emploi ne correspond � cet identifiant : " + id;
      }
    }
    catch(NumberFormatException e)
    {
      erreur = "La valeur de l'identifiant n'est pas num�rique";
    }
  }
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-th"></i> Informations sur l'offre d'emploi</h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
        <%
        if(erreur != null) // Une erreur a �t� d�tect�e et est affich�e.
        {
         %>
         <div class="row col-xs-offset-1 col-xs-10">
           <div class="panel panel-red">
             <div class="panel-heading ">
               Impossible de traiter la demande
             </div>
             <div class="panel-body text-center">
               <p class="text-danger"><strong><%=erreur%></strong></p>
             </div>
           </div>
         </div> <!-- /.row col-xs-offset-1 col-xs-10 -->
         <%
         }
        else
        {
           %>
        <div class="table-responsive">
            <small>
            <table class="table">
              <tbody>
                <tr class="success">
                  <td width="200"><strong>Identifiant</strong></td>
                  <td><%=offreemploi.getId()%></td>
                </tr>
                <tr class="warning">
                  <td><strong>Entreprise</strong></td>
                  <td><%=serviceEntreprise.nomEntreprise(offreemploi.getIdEntreprise())%>
                  	<br>
                  	<br>
                  	  <%=serviceEntreprise.descriptifEntreprise(offreemploi.getIdEntreprise())%>
                  </td>
                </tr>
                <tr class="warning">
                  <td><strong>Niveau de qualification</strong></td>
                  <td><%=offreemploi.getNiveauqualification().getIntitule()%></td>
                </tr>                
                <tr class="warning">
                  <td><strong>Description de la mission</strong></td>
                  <td><%=offreemploi.getDescriptifmission()%></td>
                </tr>
                <tr class="warning">
                  <td><strong>Profil Recherch�</strong></td>
                  <td><%=offreemploi.getProfilrecherche()%></td>
                </tr>
                <tr class="warning">
                  <td><strong>Titre</strong></td>
                  <td><%=offreemploi.getTitre()%></td>
                </tr>                                
                <tr class="warning">
                  <td><strong>Date de depot</strong></td>
                  <td><%=Utils.date2String(offreemploi.getDatedepot())%></td>
                </tr>
					<tr class="warning">
                <td><strong>Secteur d'activit�</strong></td>
                <td>
				<%
					for(Secteuractivite s : offreemploi.getSecteuractivites())
					{
				     %>
                  	   <ul><li><%=s.getIntitule()%></li></ul> <!-- Mettre une balise br au besoin -->
                	<%
					}
                %>
                </td>
                </tr>
              </tbody>
            </table>
            </small>      
        </div>
          <%
          }
        %>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
