<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                java.util.regex.Matcher,
                java.util.regex.Pattern"%>
             
<%
    IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
    IServiceCandidature serviceCandidature = (IServiceCandidature) ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");

	String input = request.getParameter("identrepriseorcandidature");
	String inputSansEspace = input.replaceAll(" ", "");
	
	/*Expression r�guli�re pour v�rifier le format de l'identifiant*/
    Pattern pattern = Pattern.compile("(ENT_|CAND_)+[0-9]{1,}"); 
    Matcher m = pattern.matcher(input);
%>

	<% if (inputSansEspace.equals("")) { %>
	<div class="row">
	  <div class="col-lg-12">
	    <div class="panel panel-default">
	      <div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw"></i>Connection</h3></div> <!-- /.panel-heading -->
	      <div class="panel-body">
			<div class="row col-xs-offset-1 col-xs-10">
	      	<div class="panel panel-red">
	      		<div class="panel-heading ">Impossible de se connecter</div>
	      		<div class="panel-body text-center">
	      			<p class="text-danger"><strong>Veuillez renseignez un identifiant pour pouvoir vous connecter</strong></p>
	      			<button type="button"class="btn btn-danger" onclick="window.location.href='template.jsp?action=connexion'">Retour...</button>
	      		</div>
	      	</div>
	      </div> <!-- /.row col-xs-offset-1 col-xs-10 --> 
	      </div> <!-- /.panel-body -->
	    </div> <!-- /.panel -->
	  </div> <!-- /.col-lg-12 -->
	</div> <!-- /.row -->
	<% }
	else if (!m.find()){ %>
	<div class="row">
	  <div class="col-lg-12">
	    <div class="panel panel-default">
	      <div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw"></i>Connection</h3></div> <!-- /.panel-heading -->
	      <div class="panel-body">
			<div class="row col-xs-offset-1 col-xs-10">
	      	<div class="panel panel-red">
	      		<div class="panel-heading ">Impossible de se connecter</div>
	      		<div class="panel-body text-center">
	      			<p class="text-danger"><strong>Identifiant non reconnu : il n'est pas de la forme CAND_XXX ou ENT_XXX</strong></p>
	      			<button type="button"class="btn btn-danger" onclick="window.location.href='template.jsp?action=connexion'">Retour...</button>
	      		</div>
	      	</div>
	      </div> <!-- /.row col-xs-offset-1 col-xs-10 --> 
	      </div> <!-- /.panel-body -->
	    </div> <!-- /.panel -->
	  </div> <!-- /.col-lg-12 -->
	</div> <!-- /.row -->
	<%
		}
	else {
		String prefixeId = input.substring(0,3);
		if (prefixeId.equals("ENT")) {
			int identifiantEntreprise = Integer.parseInt(input.substring(4));
			Entreprise entreprise = serviceEntreprise.getEntreprise(identifiantEntreprise);
	%>
		<div class="row">
		  <div class="col-lg-12">
		    <div class="panel panel-default">
		      <div class="panel-heading"><h3><i class="fa fa-angle-double-right"></i> Redirection...</h3></div> <!-- /.panel-heading -->
		      <div class="panel-body">
		      <% %>
		      <% if (serviceEntreprise.checkExistenceEntreprise(identifiantEntreprise)) {
		    	 	%>
		  		<div class="text-center">
		  			<td><strong><h3>Vous �tes bien connect�</h3></strong></td></br>
		  		</div>
		  		<div class="text-center">
		  			<button type="submit" class="btn btn-lg btn-success btn-block" onclick="window.location.href='template.jsp?action=accueil'">Revenir � l'accueil</button> <!-- � revoir -->
		  		</div>
      		  <%session.setAttribute("utilisateur",entreprise);
  			response.sendRedirect("index.jsp");
  			}%>
		      <%if (!serviceEntreprise.checkExistenceEntreprise(identifiantEntreprise)) { %>
      			<div class="row col-xs-offset-1 col-xs-10">
      			<div class="panel panel-red">
      				<div class="panel-heading ">Impossible de se connecter</div>
      					<div class="panel-body text-center">
      						<p class="text-danger"><strong>Erreur : il n'y a pas d'entreprise avec cet identifiant : <%=input%></strong></p>
      						<button type="button"class="btn btn-danger" onclick="window.location.href='template.jsp?action=connexion'">Retour...</button>
      					</div>
      				</div>
     			</div> <!-- /.row col-xs-offset-1 col-xs-10 -->    	
      		  <%}%>
		      </div> <!-- /.panel-body -->
		    </div> <!-- /.panel -->
		  </div> <!-- /.col-lg-12 -->
		</div> <!-- /.row -->
	<% 
		}
	
		else 
			prefixeId = input.substring(0,4);
			if (prefixeId.equals("CAND")) {
				int identifiantCandidature = Integer.parseInt(input.substring(5));
				Candidature candidature = serviceCandidature.getCandidature(identifiantCandidature);%>
				<div class="row">
				  <div class="col-lg-12">
				    <div class="panel panel-default">
				      <div class="panel-heading"><h3><i class="fa fa-angle-double-right"></i> Redirection...</h3></div> <!-- /.panel-heading -->
				      <div class="panel-body">
				      <% %>
				      <% if (serviceCandidature.checkExistenceCandidature(identifiantCandidature)) { 
        				%>
				  		<div class="text-center">
				  			<td><strong><h3>Vous �tes bien connect�</h3></strong></td></br>
				  		</div>
				  		<div class="text-center">
				  			<button type="submit" class="btn btn-lg btn-success btn-block" onclick="window.location.href='template.jsp?action=accueil'">Revenir � l'accueil</button> <!-- � revoir -->
				  		</div>
		      		  <%session.setAttribute("utilisateur",candidature);
						response.sendRedirect("index.jsp");
						}%>
				      <%if (!serviceCandidature.checkExistenceCandidature(identifiantCandidature)) { %>
		      			<div class="row col-xs-offset-1 col-xs-10">
		      			<div class="panel panel-red">
		      				<div class="panel-heading ">Impossible de se connecter</div>
		      					<div class="panel-body text-center">
		      						<p class="text-danger"><strong>Erreur : il n'y a pas de candidature avec cet identifiant : <%=input%></strong></p>
		      						<button type="button"class="btn btn-danger" onclick="window.location.href='template.jsp?action=connexion'">Retour...</button>
		      					</div>
		      				</div>
		     			</div> <!-- /.row col-xs-offset-1 col-xs-10 -->    	
		      		  <%}%>
				      </div> <!-- /.panel-body -->
				    </div> <!-- /.panel -->
				  </div> <!-- /.col-lg-12 -->
				</div> <!-- /.row -->
				<%
				}
		}
	%>