<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceSecteurActivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceNiveauQualification,
                java.util.LinkedList, java.util.Date, java.util.List"%>
   
             
<%
	IServiceCandidature serviceCandidature = (IServiceCandidature) ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");
	IServiceSecteurActivite serviceSecteurActivite = (IServiceSecteurActivite) ServicesLocator.getInstance().getRemoteInterface("ServiceSecteurActivite");
	IServiceNiveauQualification serviceNiveauQualification = (IServiceNiveauQualification) ServicesLocator.getInstance().getRemoteInterface("ServiceNiveauQualification");
	
	String id = request.getParameter("id");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-th"></i> Mis � jour des informations de la candidature</h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
      	
      	<% 
      	String identifiant = request.getParameter("id_candidature");
	   	String nom = request.getParameter("nom");
	 	String prenom = request.getParameter("prenom");
	 	String datenaissance = request.getParameter("datenaissance");
	 	String adressepostale = request.getParameter("adressepostale");
	 	String email = request.getParameter("email");
	 	String cv = request.getParameter("cv");
	 	String niveauQualif = request.getParameter("niveauQualif");
	 	String[] idsecteur = request.getParameterValues("idsecteur");
	 	String naissance = "";
	 	
	 	List<Integer> temp = new LinkedList<Integer>();
	 	
	 	
      	if(nom==null && prenom==null && datenaissance==null && adressepostale==null && email==null && cv==null && niveauQualif==null && idsecteur==null) {
      		try {
      	        int idCandidatureToUpdate = Integer.parseInt(id);
      	        Candidature candidature = serviceCandidature.getCandidature(idCandidatureToUpdate);
      	%>
      	
         <form action="template.jsp" method="get">
	         <input type="hidden" name="action" value="formulaire_modification_candidature" />
	         <input type="hidden" name="id_candidature" value="<%=candidature.getId()%>">
	         <div class="form-group">
				  <input type="text" class="form-control" placeholder="Identifiant" name="identifiant" value="CAND_<%=candidature.getId()%>" disabled><br>
			   </div>
	           <div class="form-group">
				  <input type="text" class="form-control" placeholder="Nom*" name="nom" value="<%=candidature.getNom()%>">
			   </div>
			   <div class="form-group">
				  <input type="text" class="form-control" placeholder="Pr�nom" name="prenom" value="<%=candidature.getPrenom()%>">
			   </div>
			   <div class="form-group">
				  <input type="text" class="form-control" placeholder="Date de naissance (format jj/mm/aaaa)" name="datenaissance" value="<%=(candidature.getDatenaissance()==null) ? naissance : Utils.date2String(candidature.getDatenaissance())%>">
			   </div>
			   <div class="form-group">
				  <input type="text" class="form-control" placeholder="Adresse postale (ville)" name="adressepostale" value="<%=candidature.getAdressepostale()%>">
			   </div>
			   <div class="form-group">
				  <input type="text" class="form-control" placeholder="Adresse email" name="email" value="<%=candidature.getAdresseemail()%>">
			   </div>
			   <div class="form-group">
				  <textarea class="form-control" placeholder="Curriculum Vit�" name="cv" rows="4" ><%=candidature.getCv()%></textarea>
			   </div>
			   
			   <div class="col-lg-3">
				   <div class="form-group">
			           <label>Niveau de qualification*</label>
			           <%
			           		String intitule = candidature.getNiveauqualification().getIntitule();
			           %>
			           <small>
			           	   <div class="radio">
				           		<label><input type="radio" name="niveauQualif" value="CAP/BEP" <% if(("CAP/BEP").equals(intitule)) {%> checked <%}%> >CAP/BEP</label>
				           </div>
				           
				           <div class="radio">
				           		<label><input type="radio" name="niveauQualif" value="Bac" <% if(("Bac").equals(intitule)) {%> checked <%}%> >Bac</label>
				           </div>
				           <div class="radio">
								<label><input type="radio" name="niveauQualif" value="Bac+3" <% if(("Bac+3").equals(intitule)) {%> checked <%}%> >Bac+3</label>
			           	   </div>
			           	   <div class="radio">
								<label><input type="radio" name="niveauQualif" value="Bac+5" <% if(("Bac+5").equals(intitule)) {%> checked <%}%> >Bac+5</label>
			           	   </div>
			           	   <div class="radio">
								<label><input type="radio" name="niveauQualif" value="Doctorat" <% if(("Doctorat").equals(intitule)) {%> checked <%}%> >Doctorat</label>
			           	   </div>
		           	   </small>
		           </div>
	           </div>
	           
	           <div class="col-lg-9">
		           <div class="form-group">
		           		<label>Secteur(s) d'activit�*</label>
		           		<%
		           			List<Integer> idSecteurActiviteAModifier =new LinkedList<Integer>();
			           		for ( Secteuractivite secteurActivite : candidature.getSecteuractivites()){
			           			idSecteurActiviteAModifier.add(secteurActivite.getId());
		           			}
		           		%>
		           		<small>
		           			<table width="100%" border="0">
		           				<tbody>
		           					<tr>
		           						<td><input type="checkbox" name="idsecteur" value="1" <% if(idSecteurActiviteAModifier.contains(1)) {%> checked <%}%> > H�tellerie/Restauration/Tourisme</td>
		           						<td><input type="checkbox" name="idsecteur" value="2" <% if(idSecteurActiviteAModifier.contains(2)) {%> checked <%}%> > Informatique</td>  
		           					</tr>
		           					<tr>
		           						<td><input type="checkbox" name="idsecteur" value="3" <% if(idSecteurActiviteAModifier.contains(3)) {%> checked <%}%> > Ressources Humaines</td>
		           						<td><input type="checkbox" name="idsecteur" value="4" <% if(idSecteurActiviteAModifier.contains(4)) {%> checked <%}%> > Assistanat/Secr�tariat</td> 
		           					</tr>
		           					<tr>
		           						<td><input type="checkbox" name="idsecteur" value="5" <% if(idSecteurActiviteAModifier.contains(5)) {%> checked <%}%> > Commercial</td>
		           						<td><input type="checkbox" name="idsecteur" value="6" <% if(idSecteurActiviteAModifier.contains(6)) {%> checked <%}%> > Finance/Banque</td>  
		           					</tr>
		           					<tr>
		           						<td><input type="checkbox" name="idsecteur" value="7" <% if(idSecteurActiviteAModifier.contains(7)) {%> checked <%}%> > T�l�com/R�seaux</td>
		           						<td><input type="checkbox" name="idsecteur" value="8" <% if(idSecteurActiviteAModifier.contains(8)) {%> checked <%}%> > Formation/Enseignement</td> 
		           						<td>&nbsp;</td>
		           					</tr>
		           				</tbody>
		           			</table>
		           		</small>
		           </div>
	           </div>
	           
			   <h1>&nbsp;</h1>
			   <h5>&nbsp;</h5>
			   <div class="text-center">
				  <button type="submit" class="btn btn-success">Valider</button>
	              <button type="reset" class="btn btn-warning">Annuler</button>
	           </div>
	           <div>
	          		<br>
	          		<em>* il est obligatoire de renseigner ces cases.</em>
	          </div>
          </form>
          
          <div class="col-lg-12 text-center">
          <br>
           	  <button class="btn btn-danger" data-toggle="modal" data-target="#mymodalSuppressionCandidature">
                <i class="glyphicon glyphicon-remove-sign fa-lg"></i> Supprimer cette candidature
              </button>
              
              <!-- Modal -->
				<div class="modal fade" id="mymodalSuppressionCandidature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">�tes-vous s�r de vouloir supprimer votre candidature ?</h4>
				      </div>
				      <div class="modal-body">
				        Cette op�ration est irr�versible... ! 
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				        <button type="button" class="btn btn-danger" onclick="window.location.href='template.jsp?action=suppression_candidature&id=<%=id%>'">Supprimer</button>
				      </div>
				    </div>
				  </div>
				</div>
	       </div>
		  <% 
      		}
      		catch (Exception e) {}
		  }
      	else {
      	  if (nom.equals("")) {%>
          	<div class="row col-xs-offset-1 col-xs-10">
            	<div class="panel panel-red">
                	<div class="panel-heading ">Impossible de traiter la demande</div>
                    <div class="panel-body text-center">
                    	<p class="text-danger">Il n'est pas possible de r�f�rencer une candidature qui ne poss�de pas de nom.</p>
                    </div>
                </div>
            </div> <!-- /.row col-xs-offset-1 col-xs-10 -->
            <% }
      	  
		  else {
			  int idCandidatureToUpdate = Integer.parseInt(identifiant);
	      	  Candidature candidature = serviceCandidature.getCandidature(idCandidatureToUpdate);
	      	 
	      	  //Recup�rer les id des secteurs d'activit� � mettre � jour
	      	  for (Secteuractivite secteurASupprimer : candidature.getSecteuractivites()) {
  	        	  temp.add(secteurASupprimer.getId());
  	          }
			  //--------------------------------------------------------------------------------------------------
			  Date dateNaissance;
			  if(!(datenaissance.equals(""))){
 	        	   dateNaissance = Utils.string2Date(datenaissance);
         	  	 }
       	 	  else {dateNaissance = null;}
			  
			  Niveauqualification niveau = serviceNiveauQualification.findNiveauQualificationByIntitule(niveauQualif);
			  Date dateDepot = new Date();
			  
			  Candidature candidatureAModifier = serviceCandidature.updateCandidature(identifiant, nom, prenom, dateNaissance, adressepostale, email, cv, idsecteur, niveau, dateDepot, temp);
			  
			  %>
        		<div class="col-lg-offset-2 col-lg-8 col-xs-12">
        			<div class="panel panel-success">
        				<div class="panel-heading">Informations mises � jour pour la candidature</div>
        			<div class="panel-body">
			        <div class="table-responsive">
			            <small>
			            <table class="table">
			              <tbody>
			                <tr class="success">
			                  <td width="200"><strong>Identifiant (login)</strong></td>
			                  <td>CAND_<%=candidatureAModifier.getId()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Niveau de qualification</strong></td>
			                  <td><%=candidatureAModifier.getNiveauqualification().getIntitule()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Nom</strong></td>
			                  <td><%=candidatureAModifier.getNom()%></td>
			                </tr>                
			                <tr class="warning">
			                  <td><strong>Pr�nom</strong></td>
			                  <td><%=candidatureAModifier.getPrenom()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Date de naissance</strong></td>
			                  <td><%=Utils.date2String(candidatureAModifier.getDatenaissance())%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Adresse postale</strong></td>
			                  <td><%=candidatureAModifier.getAdressepostale()%></td>
			                </tr>                                
			                <tr class="warning">
			                  <td><strong>Adresse e-mail</strong></td>
			                  <td><%=candidatureAModifier.getAdresseemail()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>CV</strong></td>
			                  <td><%=candidatureAModifier.getCv()%></td>
			                </tr>
			                <tr class="warning">
				  				<td><strong>Secteur(s) d'activit�</strong></td>
				  				<td>
								  <%for(Secteuractivite s : candidatureAModifier.getSecteuractivites()){%>
									 <ul><li><%=s.getIntitule()%></li></ul>
								  <%}%>
				  				</td>
			    			</tr>
			                <tr class="warning">
			                  <td><strong>Date de d�pot</strong></td>
			                  <td><%=Utils.date2String(candidatureAModifier.getDatedepot())%></td>
			                </tr>
			              </tbody>
			            </table>
			            </small>      
			        </div>
		       	</div>
		       	</div>
		       </div>

        <% } 
        }%>
      	
		  
		</div>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
