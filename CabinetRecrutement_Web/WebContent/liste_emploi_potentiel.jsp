<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                java.util.List"%>

<%
  IServiceOffreEmploi serviceOffreemploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");

  int idCandidature = Integer.parseInt(request.getParameter("idCandidature"));
  int idSA = Integer.parseInt(request.getParameter("idSA"));
  String idNQ = request.getParameter("idNQ").replaceAll(" ", "");

  
  IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="glyphicon glyphicon-transfer"></i> Liste des offres d'emploi r�f�renc�es </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
          
          <%
          	if (!(idNQ.startsWith("Ba")) || idNQ.equals("Bac")) {
          	
          		List<Offreemploi> offreemplois = serviceOffreemploi.listeDesOffreemploiCandidature(idSA,idNQ);  
          %>
            <!--
              Nom des colonnes
            -->
            <thead>
              <tr>
                <th>Identifiant</th>
                <th>Titre</th>
                <th>Entreprise</th>
                <th>Niveau de Qualification</th>
                <th>Date de d�pot</th>
                <th>Message</th>
                <th>Informations</th>
              </tr>
            </thead>
            <!--
              Contenu du tableau
            -->
            <tbody>
              <%
              for(Offreemploi offreemploi : offreemplois)
              {
                %>
                <tr>
                 <td><%=offreemploi.getId()%></td>
                 <td><%=offreemploi.getTitre()%></td>
                 <td><%=serviceEntreprise.nomEntreprise(offreemploi.getIdEntreprise())%></td>
                 <td><%=offreemploi.getNiveauqualification().getIntitule()%></td>
                 <td><%=Utils.date2String(offreemploi.getDatedepot())%></td>
                 <td align="center"><a href="template.jsp?action=nouveau_message_candidature&idCandidature=<%=idCandidature%>&idOffreEmploi=<%=offreemploi.getId()%>"><i class="fa fa-envelope fa-fw"></i></a></td> 
                 <td align="center"><a href="template.jsp?action=infos_offreemploi&id=<%=offreemploi.getId()%>"><i class="fa fa-eye fa-lg"></i></a></td>
                </tr>
                <%
              }
              %>
            </tbody>
            <% } 
             else {
            	  String prefixeIdNQ = idNQ.substring(0, 3);
             	  String suffixeIdNQ = idNQ.substring(3);
             	  String idNQfinal = prefixeIdNQ+"+"+suffixeIdNQ; //car le format de idNQ r�cup�rer n'est pas bon!
             	  
             	  List<Offreemploi> offreemplois = serviceOffreemploi.listeDesOffreemploiCandidature(idSA,idNQfinal);
            	 
            %>
            <!--
              Nom des colonnes
            -->
            <thead>
              <tr>
                <th>Identifiant</th>
                <th>Titre</th>
                <th>Entreprise</th>
                <th>Niveau de Qualification</th>
                <th>Date de d�pot</th>
                <th>Message</th>
                <th>Informations</th>
              </tr>
            </thead>
            <!--
              Contenu du tableau
            -->
            <tbody>
              <%
              for(Offreemploi offreemploi : offreemplois)
              {
                %>
                <tr>
                 <td><%=offreemploi.getId()%></td>
                 <td><%=offreemploi.getTitre()%></td>
                 <td><%=serviceEntreprise.nomEntreprise(offreemploi.getIdEntreprise())%></td>
                 <td><%=offreemploi.getNiveauqualification().getIntitule()%></td>
                 <td><%=Utils.date2String(offreemploi.getDatedepot())%></td>
                 <td align="center"><a href="template.jsp?action=nouveau_message_candidature&idCandidature=<%=idCandidature%>&idOffreEmploi=<%=offreemploi.getId()%>"><i class="fa fa-envelope fa-fw"></i></a></td>
                 <td align="center"><a href="template.jsp?action=infos_offreemploi&id=<%=offreemploi.getId()%>"><i class="fa fa-eye fa-lg"></i></a></td>
                </tr>
                <%
              }
              %>
            </tbody>
            <% } %>
          </table>
        </div> <!-- /.table-responsive -->
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
