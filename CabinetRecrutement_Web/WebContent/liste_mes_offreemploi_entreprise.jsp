<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                java.util.List,
                java.util.Set,
                java.util.HashSet,
                java.util.LinkedList"%>

<%

  IServiceOffreEmploi serviceOffreemploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
  IServiceCandidature serviceCandidature = (IServiceCandidature) ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature"); 
  List<Offreemploi> offreemplois = serviceOffreemploi.listeDesOffreemploiEntreprise(Integer.parseInt(request.getParameter("id")));
  
  IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="glyphicon glyphicon-transfer"></i> Liste des offres d'emploi r�f�renc�es </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <!--
              Nom des colonnes
            -->
            <thead>
              <tr>
                <th>Identifiant</th>
                <th>Titre</th>
                <th>Entreprise</th>
                <th>Niveau de Qualification</th>
                <th>Date de d�pot</th>
                <th>Candidatures Potentielles</th>
                <th>Modification</th>
                <th>Informations</th>
              </tr>
            </thead>
            <!--
              Contenu du tableau
            -->
            <tbody>
              <%
              for(Offreemploi offreemploi : offreemplois)
              {
                %>
                <tr>
                 <td><%=offreemploi.getId()%></td>
                 <td><%=offreemploi.getTitre()%></td>
                 <td><%=serviceEntreprise.nomEntreprise(offreemploi.getIdEntreprise())%></td>
                 <td><%=offreemploi.getNiveauqualification().getIntitule()%></td>
                 <td><%=Utils.date2String(offreemploi.getDatedepot())%></td> 
                 <td align="center">
                 	<table width="100%">
                 	<tbody>
                 	<tr>
                 	<%
                 	
                 	List<Candidature> candidatsPotentiels = new LinkedList<Candidature>();
                 	for (Secteuractivite s:offreemploi.getSecteuractivites()){
                 		candidatsPotentiels = serviceOffreemploi.findCandidaturesPotentielleBySecteurActivite(s.getId(), offreemploi.getNiveauqualification().getIntitule());
                 	}
                 	
                 	Set<Candidature> candidatsPotentielsGlobal = new HashSet<Candidature>(candidatsPotentiels);
                 	for (Candidature candidat: candidatsPotentielsGlobal ){ 
                 			
                 			%>
                 			<tr>
                 			<td><a href="template.jsp?action=infos_candidature&id=<%=candidat.getId()%>"><%=candidat.getNom()%></a></td>
                 			<td align="right"><a href="template.jsp?action=nouveau_message_offre_emploi&idOffre=<%=offreemploi.getId()%>&idCandidat=<%=candidat.getId()%>"><i class="fa fa-envelope fa-fw"></i></a></td>
                 			</tr>
                 			<%
                 		}
                 	%>
                 	
                 	</tr>
                 	
                 	</tbody>
                 	</table>
            
                 </td>
                 
                 <td align="center"><a href="template.jsp?action=maj_offre&id_offre=<%=offreemploi.getId()%>"><i class="fa fa-pencil-square-o fa-lg"></i></a></td>
                  <td align="center"><a href="template.jsp?action=infos_offreemploi&id=<%=offreemploi.getId()%>"><i class="fa fa-eye fa-lg"></i></a></td>
                </tr>
                <%
              }
              %>
            </tbody>
          </table>
        </div> <!-- /.table-responsive -->
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
