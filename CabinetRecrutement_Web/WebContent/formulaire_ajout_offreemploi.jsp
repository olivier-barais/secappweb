<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceOffreEmploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceSecteurActivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceNiveauQualification,
                java.util.Date"%>
             
<%
    IServiceOffreEmploi serviceOffreEmploi = (IServiceOffreEmploi) ServicesLocator.getInstance().getRemoteInterface("ServiceOffreEmploi");
	IServiceSecteurActivite serviceSecteurActivite = (IServiceSecteurActivite) ServicesLocator.getInstance().getRemoteInterface("ServiceSecteurActivite");
	IServiceNiveauQualification serviceNiveauQualification = (IServiceNiveauQualification) ServicesLocator.getInstance().getRemoteInterface("ServiceNiveauQualification");
	IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
	
	String id = request.getParameter("idEntreprise");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-users"></i> R�f�rencer une nouvelle offre d'emploi </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
      	
      	<%
      	String idEnt = request.getParameter("id_entreprise");
      	String titreOffre = request.getParameter("titre");
    	String descriptifMission = request.getParameter("descriptifmission");
    	String profilRecherche = request.getParameter("profilrecherche");
    	String niveauQualif = request.getParameter("niveauQualif");
    	String[] idsecteur = request.getParameterValues("idsecteur");
    	
    	if (titreOffre ==null && niveauQualif==null && idsecteur==null) {
      		try {
      	        int idEntrepriseToCreate = Integer.parseInt(id);
      	        Entreprise entreprise = serviceEntreprise.getEntreprise(idEntrepriseToCreate);%>
      	
         <form action="" method="get">
         <input type="hidden" name="action" value="formulaire_ajout_offreemploi" />
         <input type="hidden" name="id_entreprise" value="<%=entreprise.getId()%>">
         
           <div class="form-group">
			  <input type="text" class="form-control" placeholder="Titre de l'offre*" name="titre">
		   </div>
		   <div class="form-group">
			  <textarea class="form-control" placeholder="Description de la mission" name="descriptifmission" rows="4"></textarea><br>
		   </div>
		   <div class="form-group">
			  <textarea class="form-control" placeholder="Profil recherch�" name="profilrecherche" rows="4"></textarea><br>
		   </div>
		   
		   <div class="col-lg-3">
			   <div class="form-group">
		           <label>Niveau de qualification*</label>
		           <small>
			           <div class="radio">
			           		<label><input type="radio" name="niveauQualif" value="CAP/BEP">CAP/BEP</label>
			           </div>
			           <div class="radio">
			           		<label><input type="radio" name="niveauQualif" value="Bac">Bac</label>
			           </div>
			           <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Bac+3">Bac+3</label>
		           	   </div>
		           	   <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Bac+5">Bac+5</label>
		           	   </div>
		           	   <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Doctorat">Doctorat</label>
		           	   </div>
	           	   </small>
	           </div>
           </div>
           
           <div class="col-lg-9">
	           <div class="form-group">
	           		<label>Secteur(s) d'activit�*</label>
	           		<small>
	           			<table width="100%" border="0">
	           				<tbody>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="1"> H�tellerie/Restauration/Tourisme</td>
	           						<td><input type="checkbox" name="idsecteur" value="2"> Informatique</td>  
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="3"> Ressources Humaines</td>
	           						<td><input type="checkbox" name="idsecteur" value="4"> Assistanat/Secr�tariat </td> 
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="5"> Commercial</td>
	           						<td><input type="checkbox" name="idsecteur" value="6"> Finance/Banque</td>  
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="7"> T�l�com/R�seaux</td>
	           						<td><input type="checkbox" name="idsecteur" value="8"> Formation/Enseignement</td> 
	           						<td>&nbsp;</td>
	           					</tr>
	           				</tbody>
	           			</table>
	           		</small>
	           </div>
           </div>
           
		   <h1>&nbsp;</h1>
		   <h5>&nbsp;</h5>
		   <div class="text-center">
			  <button type="submit" class="btn btn-success">Cr�er</button>
              <button type="reset" class="btn btn-warning">Annuler la saisie</button>
           </div>
           <div>
          		<br>
          		<em>* il est obligatoire de renseigner ces cases.</em>
          </div>
          </form>
          
          <%}
      		catch (Exception e) {}
      	}
          else {
        	  if (titreOffre.equals("")) {%>
            	<div class="row col-xs-offset-1 col-xs-10">
              	<div class="panel panel-red">
                  	<div class="panel-heading ">Impossible de traiter la demande</div>
                      <div class="panel-body text-center">
                      	<p class="text-danger">Il n'est pas possible de r�f�rencer une offre d'emploi qui ne poss�de pas de titre.</p>
                      </div>
                  </div>
              </div> <!-- /.row col-xs-offset-1 col-xs-10 -->
              <% }
        	  
        	  else {
        	  	 Niveauqualification niveau = serviceNiveauQualification.findNiveauQualificationByIntitule(niveauQualif);
        	 	 Date dateDepot = new Date();
        	 	 int idEntreprise = Integer.parseInt(idEnt);
        	 	 
        	 	 Offreemploi offreEmploi = serviceOffreEmploi.nouvelleOffreEmploi(idEntreprise, titreOffre, descriptifMission, profilRecherche, niveau, idsecteur, dateDepot);
          %>
          		<div class="col-lg-offset-2 col-lg-8 col-xs-12">
          			<div class="panel panel-success">
          				<div class="panel-heading">Nouvelle offre d'emploi r�f�renc�e</div>
          			<div class="panel-body">
			        <div class="table-responsive">
			            <small>
			            <table class="table">
			              <tbody>
			                <tr class="success">
			                  <td width="200"><strong>Identifiant</strong></td>
			                  <td><%=offreEmploi.getId()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Entreprise</strong></td>
			                  <td><%=serviceEntreprise.nomEntreprise(offreEmploi.getIdEntreprise())%>
			                  	<br>
			                  	<br>
			                  	  <%=serviceEntreprise.descriptifEntreprise(offreEmploi.getIdEntreprise())%>
			                  </td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Niveau de qualification</strong></td>
			                  <td><%=offreEmploi.getNiveauqualification().getIntitule()%></td>
			                </tr>                
			                <tr class="warning">
			                  <td><strong>Description de la mission</strong></td>
			                  <td><%=offreEmploi.getDescriptifmission()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Profil Recherch�</strong></td>
			                  <td><%=offreEmploi.getProfilrecherche()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Titre</strong></td>
			                  <td><%=offreEmploi.getTitre()%></td>
			                </tr>                                
			                <tr class="warning">
			                  <td><strong>Date de depot</strong></td>
			                  <td><%=Utils.date2String(offreEmploi.getDatedepot())%></td>
			                </tr>
								<tr class="warning">
			                <td><strong>Secteur d'activit�</strong></td>
			                <td>
							<%
								for(Secteuractivite s : offreEmploi.getSecteuractivites())
								{
							     %>
			                  	   <ul><li><%=s.getIntitule()%></li></ul> <!-- Mettre une balise br au besoin -->
			                	<%
								}
			                %>
			                </td>
			                </tr>
			              </tbody>
			            </table>
			            </small>      
			        </div>
		       	</div>
		       	</div>
		       </div>
				
          <%}
        	  }%>
		</div>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
