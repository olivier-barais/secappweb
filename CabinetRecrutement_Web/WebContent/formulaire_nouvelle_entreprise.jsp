<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-th"></i> Cr�ation d'une nouvelle entreprise </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
         <form action="template.jsp?action=nouvelle_entreprise" method="get">
         <input type="hidden" name="action" value="nouvelle_entreprise" />
           <div class="form-group">
			  <input type="text" class="form-control" placeholder="Nom de l'entreprise" name="nomentreprise"><br>
		   </div>
		   
		   <div class="form-group">
			  <textarea class="form-control" placeholder="Description de l'entreprise" name="descriptifentreprise" rows="4"></textarea><br>
		   </div>
		   
		   <div class="form-group">
			  <input type="text" class="form-control" placeholder="Adresse postale de l'entreprise (ville)" name="adressepostale"><br>
		   </div>
		   
		   <div class="text-center">
			  <button type="submit" class="btn btn-success">Cr�er</button>
              <button type="reset" class="btn btn-warning">Annuler la saisie</button>
           </div>
          </form>
		</div>
 
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
