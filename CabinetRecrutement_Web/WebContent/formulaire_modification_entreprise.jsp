<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceEntreprise,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise"%>
   
             
<%
    IServiceEntreprise serviceEntreprise = (IServiceEntreprise) ServicesLocator.getInstance().getRemoteInterface("ServiceEntreprise");
	Entreprise entrepriseToUpdate = new Entreprise();
    
	String id = request.getParameter("id");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-th"></i> Mis � jour des informations de l'entreprise</h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
      	
      	<% 
      	String identifiant = request.getParameter("id_entreprise");
      	String nomEntreprise = request.getParameter("nomentreprise");
   	    String descriptif = request.getParameter("descriptifentreprise");
   	    String adressePostale = request.getParameter("adressepostale");
   	    
      	if(nomEntreprise == null && descriptif == null && adressePostale == null) {
      		try {
      	        int idEntrepriseToUpdate = Integer.parseInt(id);
      	        Entreprise entreprise = serviceEntreprise.getEntreprise(idEntrepriseToUpdate);
      	%>
         <form role ="form" action="template.jsp" method="get">
         <input type="hidden" name="action" value="formulaire_modification_entreprise" />
         <input type="hidden" name="id_entreprise" value="<%=entreprise.getId()%>">
           <div class="form-group">
			  <input type="text" class="form-control" placeholder="Identifiant" name="identifiant" value="ENT_<%=entreprise.getId()%>" disabled><br>
		   </div>
           <div class="form-group">
			  <input type="text" class="form-control" placeholder="Nom de l'entreprise" name="nomentreprise" value="<%=entreprise.getNom()%>"><br>
		   </div>
		   
		   <div class="form-group">
			  <textarea class="form-control" placeholder="Description de l'entreprise" name="descriptifentreprise" rows="4"><%=entreprise.getDescriptif()%></textarea><br>
		   </div>
		   
		   <div class="form-group">
			  <input type="text" class="form-control" placeholder="Adresse postale de l'entreprise" name="adressepostale" value="<%=entreprise.getAdressePostale()%>"><br>
		   </div>
		   
		   <div class="text-center">
			  <button type="submit" class="btn btn-success" name="validermaj">Valider</button>
              <button type="reset" class="btn btn-warning" name="annulermaj">Annuler</button>
           </div>
          </form>
          
          <div class="col-lg-12 text-center">
          <br>
           	  <button class="btn btn-danger" data-toggle="modal" data-target="#mymodalSuppressionEntreprise">
                <i class="glyphicon glyphicon-remove-sign fa-lg"></i> Supprimer cette entreprise
              </button>
              
              <!-- Modal -->
				<div class="modal fade" id="mymodalSuppressionEntreprise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">�tes-vous s�r de vouloir supprimer votre entreprise 
				        	ainsi que tous les offres d'emploi et messages associ�es ?</h4>
				      </div>
				      <div class="modal-body">
				        Cette op�ration n'est pas r�versible... ! 
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				        <button type="button" class="btn btn-danger" onclick="window.location.href='template.jsp?action=suppression_entreprise&id=<%=id%>'">Supprimer</button>
				      </div>
				    </div>
				  </div>
				</div>
	       </div>
		  <% 
      		}
      		catch (Exception e) {}
		  } 
		  else {
			  entrepriseToUpdate.setId(Integer.parseInt(identifiant));
			  entrepriseToUpdate.setNom(nomEntreprise);
			  entrepriseToUpdate.setDescriptif(descriptif);
			  entrepriseToUpdate.setAdressePostale(adressePostale);
			  Entreprise entrepriseModifier = serviceEntreprise.updateEntreprise(entrepriseToUpdate);
			  %>
			  <div class="col-lg-offset-2 col-lg-8 col-xs-12">
        			<div class="panel panel-success">
        				<div class="panel-heading">Informations mises � jour pour l'entreprise</div>
        			<div class="panel-body">
			  <div class="table-responsive">
	            <small>
	            <table class="table">
	              <tbody>
	                <tr class="success">
	                  <td width="200"><strong>Identifiant (login)</strong></td>
	                  <td>ENT_<%=entrepriseModifier.getId()%></td>
	                </tr>
	                <tr class="warning">
	                  <td><strong>Nom</strong></td>
	                  <td><%=entrepriseModifier.getNom()%></td>
	                </tr>
	                <tr class="warning">
	                  <td><strong>Adresse postale (ville)</strong></td>
	                  <td><%=entrepriseModifier.getAdressePostale()%></td>
	                </tr>
	                <tr class="warning">
	                  <td><strong>Descriptif</strong></td>
	                  <td><%=Utils.text2HTML(entrepriseModifier.getDescriptif())%></td>
	                </tr>
	              </tbody>
	            </table>
	            </small>      
	        </div>
	        </div>
	        </div>
	        </div>
	        
		  <% } %>
      	
		  
		</div>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
