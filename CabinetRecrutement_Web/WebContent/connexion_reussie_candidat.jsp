<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                java.util.List,
                java.util.regex.Matcher,
                java.util.regex.Pattern"%>
             
<%
    IServiceCandidature serviceCandidature = (IServiceCandidature) ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");

	String input = request.getParameter("idnamecandidat");
	
	/*Expression r�guli�re pour v�rifier le format de l'identifiant*/
    //Pattern pattern = Pattern.compile("(ENT_|CAND_)+[0-9]{1,}"); 
    //Matcher m = pattern.matcher(input);
%>

	<% if (input.equals("")) { %>
	<div class="row">
	  <div class="col-lg-12">
	    <div class="panel panel-default">
	      <div class="panel-heading"><h3><i class="fa fa-sign-in  fa-fw"></i>Connection</h3></div> <!-- /.panel-heading -->
	      <div class="panel-body">
			<div class="row col-xs-offset-1 col-xs-10">
	      	<div class="panel panel-red">
	      		<div class="panel-heading ">Impossible de se connecter</div>
	      		<div class="panel-body text-center">
	      			<p class="text-danger"><strong>Veuillez renseignez un identifiant pour pouvoir vous connecter</strong></p>
	      			<button type="button"class="btn btn-danger" onclick="window.location.href='template.jsp?action=connexion_candidat_par_nom'">Retour...</button>
	      		</div>
	      	</div>
	      </div> <!-- /.row col-xs-offset-1 col-xs-10 --> 
	      </div> <!-- /.panel-body -->
	    </div> <!-- /.panel -->
	  </div> <!-- /.col-lg-12 -->
	</div> <!-- /.row -->
	<% }
		else {
				List<Candidature> listCandidat = serviceCandidature.checkExistenceCandidatureName(input); 
				for (Candidature candidat : listCandidat) {%> 
					<div class="row">
					  <div class="col-lg-12">
					    <div class="panel panel-default">
					      <div class="panel-heading"><h3><i class="fa fa-th"></i> Informations sur la candidature</h3></div> <!-- /.panel-heading -->
					      <div class="panel-body">

					        <div class="table-responsive">
					            <small>
					            <table class="table">
					              <tbody>
					                <tr class="success">
					                  <td width="200"><strong>Identifiant (login)</strong></td>
					                  <td>CAND_<%=candidat.getId()%></td>
					                </tr>
					                <tr class="warning">
					                  <td><strong>Niveau de qualification</strong></td>
					                  <td><%=candidat.getNiveauqualification().getIntitule()%></td>
					                </tr>
					                <tr class="warning">
					                  <td><strong>Nom</strong></td>
					                  <td><%=candidat.getNom()%></td>
					                </tr>                
					                <tr class="warning">
					                  <td><strong>Pr�nom</strong></td>
					                  <td><%=candidat.getPrenom()%></td>
					                </tr>
					                <tr class="warning">
					                  <td><strong>Mot de passe</strong></td>
					                  <td><%=Utils.text2HTML("Eventuel mot de passe hach� ou non (^-^)")%></td>
					                </tr>
					                <tr class="warning">
					                  <td><strong>Date de naissance</strong></td>
					                  <td><%=Utils.date2String(candidat.getDatenaissance())%></td>
					                </tr>
					                <tr class="warning">
					                  <td><strong>Adresse postale</strong></td>
					                  <td><%=candidat.getAdressepostale()%></td>
					                </tr>                                
					                <tr class="warning">
					                  <td><strong>Adresse e-mail</strong></td>
					                  <td><%=candidat.getAdresseemail()%></td>
					                </tr>
					                <tr class="warning">
					                  <td><strong>CV</strong></td>
					                  <td><%=candidat.getCv()%></td>
					                </tr>
					                <tr class="warning">
									  <td><strong>Secteur(s) d'activit�</strong></td>
									  <td>
										  <%for(Secteuractivite s : candidat.getSecteuractivites()){%>
											 <ul><li><%=s.getIntitule()%></li></ul> <!-- Mettre une balise br au besoin -->
										  <%}%>
									  </td>
								    </tr>
					                <tr class="warning">
					                  <td><strong>Date de d�pot</strong></td>
					                  <td><%=Utils.date2String(candidat.getDatedepot())%></td>
					                </tr>
					              </tbody>
					            </table>
					            </small>      
					        </div>
					        
					      </div> <!-- /.panel-body -->
					    </div> <!-- /.panel -->
					  </div> <!-- /.col-lg-12 -->
					</div> <!-- /.row -->
				<%} %>
				
		<%}
	%>