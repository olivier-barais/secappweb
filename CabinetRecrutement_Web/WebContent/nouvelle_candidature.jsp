<%@ page language="java" contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@page import="eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator,
                eu.telecom_bretagne.cabinet_recrutement.front.utils.Utils,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceCandidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification,
                eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceSecteurActivite,
                eu.telecom_bretagne.cabinet_recrutement.service.IServiceNiveauQualification,
                java.util.Date, java.util.Set,
                java.util.HashSet"%>
             
<%
    IServiceCandidature serviceCandidature = (IServiceCandidature) ServicesLocator.getInstance().getRemoteInterface("ServiceCandidature");
	IServiceSecteurActivite serviceSecteurActivite = (IServiceSecteurActivite) ServicesLocator.getInstance().getRemoteInterface("ServiceSecteurActivite");
	IServiceNiveauQualification serviceNiveauQualification = (IServiceNiveauQualification) ServicesLocator.getInstance().getRemoteInterface("ServiceNiveauQualification");
    
	String nom = request.getParameter("nom");
	String prenom = request.getParameter("prenom");
	String datenaissance = request.getParameter("datenaissance");
	String adressepostale = request.getParameter("adressepostale");
	String email = request.getParameter("email");
	String cv = request.getParameter("cv");
	String niveauQualif = request.getParameter("niveauQualif");
	String[] idsecteur = request.getParameterValues("idsecteur");
%>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><h3><i class="fa fa-users"></i> R�f�rencer une nouvelle candidature </h3></div> <!-- /.panel-heading -->
      <div class="panel-body">
      	<div class="col-lg-offset-2 col-lg-8 col-xs-12">
      	
      	
      	
      	<%if (niveauQualif==null && idsecteur==null) {%>
      	
         <form action="" method="get">
         <input type="hidden" name="action" value="nouvelle_candidature" />
           <div class="form-group">
			  <input type="text" class="form-control" placeholder="Nom*" name="nom">
		   </div>
		   <div class="form-group">
			  <input type="text" class="form-control" placeholder="Pr�nom" name="prenom">
		   </div>
		   <div class="form-group">
			  <input type="text" class="form-control" placeholder="Date de naissance (format jj/mm/aaaa)" name="datenaissance">
		   </div>
		   <div class="form-group">
			  <input type="text" class="form-control" placeholder="Adresse postale (ville)" name="adressepostale">
		   </div>
		   <div class="form-group">
			  <input type="text" class="form-control" placeholder="Adresse email" name="email">
		   </div>
		   <div class="form-group">
			  <textarea class="form-control" placeholder="Curriculum Vit�" name="cv" rows="4"></textarea>
		   </div>
		   
		   <div class="col-lg-3">
			   <div class="form-group">
		           <label>Niveau de qualification*</label>
		           <small>
			           <div class="radio">
			           		<label><input type="radio" name="niveauQualif" value="CAP/BEP">CAP/BEP</label>
			           </div>
			           <div class="radio">
			           		<label><input type="radio" name="niveauQualif" value="Bac">Bac</label>
			           </div>
			           <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Bac+3">Bac+3</label>
		           	   </div>
		           	   <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Bac+5">Bac+5</label>
		           	   </div>
		           	   <div class="radio">
							<label><input type="radio" name="niveauQualif" value="Doctorat">Doctorat</label>
		           	   </div>
	           	   </small>
	           </div>
           </div>
           
           <div class="col-lg-9">
	           <div class="form-group">
	           		<label>Secteur(s) d'activit�*</label>
	           		<small>
	           			<table width="100%" border="0">
	           				<tbody>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="1"> H�tellerie/Restauration/Tourisme</td>
	           						<td><input type="checkbox" name="idsecteur" value="2"> Informatique</td>  
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="3"> Ressources Humaines</td>
	           						<td><input type="checkbox" name="idsecteur" value="4"> Assistanat/Secr�tariat </td> 
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="5"> Commercial</td>
	           						<td><input type="checkbox" name="idsecteur" value="6"> Finance/Banque</td>  
	           					</tr>
	           					<tr>
	           						<td><input type="checkbox" name="idsecteur" value="7"> T�l�com/R�seaux</td>
	           						<td><input type="checkbox" name="idsecteur" value="8"> Formation/Enseignement</td> 
	           						<td>&nbsp;</td>
	           					</tr>
	           				</tbody>
	           			</table>
	           		</small>
	           </div>
           </div>
           
		   <h1>&nbsp;</h1>
		   <h5>&nbsp;</h5>
		   <div class="text-center">
			  <button type="submit" class="btn btn-success">Cr�er</button>
              <button type="reset" class="btn btn-warning">Annuler la saisie</button>
           </div>
           <div>
          		<br>
          		<em>* il est obligatoire de renseigner ces cases.</em>
          </div>
          </form>
          
          <%} 
          else {
        	  if (nom.equals("")) {%>
            	<div class="row col-xs-offset-1 col-xs-10">
              	<div class="panel panel-red">
                  	<div class="panel-heading ">Impossible de traiter la demande</div>
                      <div class="panel-body text-center">
                      	<p class="text-danger">Il n'est pas possible de r�f�rencer une candidature qui ne poss�de pas de nom.</p>
                      </div>
                  </div>
              </div> <!-- /.row col-xs-offset-1 col-xs-10 -->
              <% }
        	  
        	  else {
        	  	 Niveauqualification niveau = serviceNiveauQualification.findNiveauQualificationByIntitule(niveauQualif);
        	  
        	 	 Date dateDepot = new Date();
        	 	 Date dateNaissance;
        	 	 
        	 	 if(!(datenaissance.equals(""))){
          		 
  	        	   dateNaissance = Utils.string2Date(datenaissance);
          	  	 }
        	 	 else {
        	 		dateNaissance = null;
        	 	 }
        	 	 
        	 	 Candidature candidature = serviceCandidature.nouvelleCandidature(nom, prenom, dateNaissance, adressepostale, email, cv, idsecteur, niveau, dateDepot);
        	 	 
          %>
          		<div class="col-lg-offset-2 col-lg-8 col-xs-12">
          			<div class="panel panel-success">
          				<div class="panel-heading">Nouvelle candidature r�f�renc�e</div>
          			<div class="panel-body">
			        <div class="table-responsive">
			            <small>
			            <table class="table">
			              <tbody>
			                <tr class="success">
			                  <td width="200"><strong>Identifiant (login)</strong></td>
			                  <td>CAND_<%=candidature.getId()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Niveau de qualification</strong></td>
			                  <td><%=candidature.getNiveauqualification().getIntitule()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Nom</strong></td>
			                  <td><%=candidature.getNom()%></td>
			                </tr>                
			                <tr class="warning">
			                  <td><strong>Pr�nom</strong></td>
			                  <td><%=candidature.getPrenom()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Date de naissance</strong></td>
			                  <td><%=Utils.date2String(candidature.getDatenaissance())%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>Adresse postale</strong></td>
			                  <td><%=candidature.getAdressepostale()%></td>
			                </tr>                                
			                <tr class="warning">
			                  <td><strong>Adresse e-mail</strong></td>
			                  <td><%=candidature.getAdresseemail()%></td>
			                </tr>
			                <tr class="warning">
			                  <td><strong>CV</strong></td>
			                  <td><%=candidature.getCv()%></td>
			                </tr>
			                <tr class="warning">
				  				<td><strong>Secteur(s) d'activit�</strong></td>
				  				<td>
								  <%for(Secteuractivite s : candidature.getSecteuractivites()){%>
									 <ul><li><%=s.getIntitule()%></li></ul>
								  <%}%>
				  				</td>
			    			</tr>
			                <tr class="warning">
			                  <td><strong>Date de d�pot</strong></td>
			                  <td><%=Utils.date2String(candidature.getDatedepot())%></td>
			                </tr>
			              </tbody>
			            </table>
			            </small>      
			        </div>
		       	</div>
		       	</div>
		       </div>

          <%}
        	  }%>
		</div>
      </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
  </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
