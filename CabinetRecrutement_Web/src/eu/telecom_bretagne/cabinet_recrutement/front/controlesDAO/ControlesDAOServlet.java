package eu.telecom_bretagne.cabinet_recrutement.front.controlesDAO;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.telecom_bretagne.cabinet_recrutement.data.dao.EntrepriseDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.NiveauqualificationDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.OffreemploiDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.SecteuractiviteDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.dao.CandidatureDAO;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Candidature;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Entreprise;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Niveauqualification;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Offreemploi;
import eu.telecom_bretagne.cabinet_recrutement.data.model.Secteuractivite;
import eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocator;
import eu.telecom_bretagne.cabinet_recrutement.front.utils.ServicesLocatorException;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/ControlesDAO")
public class ControlesDAOServlet extends HttpServlet
{
  //-----------------------------------------------------------------------------
  private static final long serialVersionUID = 1L;
  //-----------------------------------------------------------------------------
  /**
   * @see HttpServlet#HttpServlet()
   */
  public ControlesDAOServlet()
  {
    super();
  }
  //-----------------------------------------------------------------------------
  /**
   * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
   */
  @SuppressWarnings("deprecation")
protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    // Flot de sortie pour écriture des résultats.
    PrintWriter out = response.getWriter();
    
    // Récupération de la réféence vers le(s) DAO(s)
    EntrepriseDAO entrepriseDAO = null;
    CandidatureDAO candidatureDAO = null;
    OffreemploiDAO offreemploiDAO = null;
    SecteuractiviteDAO secteuractiviteDAO = null;
    NiveauqualificationDAO niveauqualificationDAO = null;
    
    try
    {
      entrepriseDAO = (EntrepriseDAO) ServicesLocator.getInstance().getRemoteInterface("EntrepriseDAO");
      candidatureDAO = (CandidatureDAO) ServicesLocator.getInstance().getRemoteInterface("CandidatureDAO");
      offreemploiDAO = (OffreemploiDAO) ServicesLocator.getInstance().getRemoteInterface("OffreemploiDAO");
      secteuractiviteDAO = (SecteuractiviteDAO) ServicesLocator.getInstance().getRemoteInterface("SecteuractiviteDAO");
      niveauqualificationDAO = (NiveauqualificationDAO) ServicesLocator.getInstance().getRemoteInterface("NiveauqualificationDAO");
    }
    catch (ServicesLocatorException e)
    {
      e.printStackTrace();
    }
    out.println("Contrôles de fonctionnement du DAO EntrepriseDAO");
    out.println();
    
    // Contrôle(s) de fonctionnalités.
    
    out.println("Liste des entreprises :");
    List<Entreprise> entreprises = entrepriseDAO.findAll();
    
    for(Entreprise entreprise : entreprises)
    {
      out.println(entreprise.getNom());
    }
    out.println();
    
    out.println("Obtention de l'entreprise n° 1 :");
    Entreprise e = entrepriseDAO.findById(1);
    out.println(e.getId());
    out.println(e.getNom());
    out.println(e.getDescriptif());
    out.println(e.getAdressePostale());
    out.println();

    out.println("Obtention de l'entreprise n° 2 :");
    e = entrepriseDAO.findById(2);
    out.println(e.getId());
    out.println(e.getNom());
    out.println(e.getDescriptif());
    out.println(e.getAdressePostale());
    out.println();
    /*
    Entreprise e_test = new Entreprise();
    e_test.setId(4);
    e_test.setNom("Entreprise Test");
    e_test.setDescriptif("Descriptif de test");
    e_test.setAdressePostale("BREST");
    entrepriseDAO.persist(e_test);
    
    Entreprise e_test2 = new Entreprise();
    e_test2.setId(4);
    e_test2.setNom("Entreprise Test");
    e_test2.setDescriptif("Descriptif de test");
    e_test2.setAdressePostale("PLOUZANE");
    entrepriseDAO.update(e_test2);
    */

    
    Niveauqualification niveauqualification = new Niveauqualification();
    niveauqualification.setIntitule("bac2");
    //niveauqualificationDAO.persist(niveauqualification);
    
    
    Offreemploi offreemploi = new Offreemploi();
    offreemploi.setId(3);
    offreemploi.setIdEntreprise(3);
    offreemploi.setNiveauqualification(niveauqualification);
    offreemploi.setDescriptifmission("blablabla");
    offreemploi.setProfilrecherche("compétent");
    offreemploi.setTitre("cdd technicien");
    offreemploi.setDatedepot(new Date(2020,9,9));
    //offreemploiDAO.persist(offreemploi);
    
    out.println(offreemploi.getSecteuractivites());
    
    out.println("Contrôles de fonctionnement du DAO CandidatureDAO");
    out.println();
    

    Candidature candidature_test = new Candidature();
    candidature_test.setId(4);
    candidature_test.setNiveauqualification(niveauqualification);
    candidature_test.setNom("Titi");
    candidature_test.setPrenom("toto");
    candidature_test.setDatenaissance(new Date(1990,10,05));
    candidature_test.setAdressepostale("BREST");
    candidature_test.setAdresseemail("titi@toto.tata");
    candidature_test.setCv("coucou ,c'est titi");
    candidature_test.setDatedepot(new Date(2020,04,03));
    //candidatureDAO.persist(candidature_test);
    
    Secteuractivite secteuractivite = new Secteuractivite();
    secteuractivite.setId(6);
    secteuractivite.setIntitule("Cyberguerre");
    //secteuractiviteDAO.persist(secteuractivite);
    
    
    
    // Contrôle(s) de fonctionnalités.
    
    out.println("Liste des candidatures :");
    List<Candidature> candidatures = candidatureDAO.findAll();
    
    for(Candidature candidature : candidatures)
    {
      out.println(candidature.getNom()); //a completer
    }
    out.println();
    
    out.println("Obtention des candidatures :");
    List <Candidature> c = candidatureDAO.findBySecteurActiviteAndNiveauQualification(2, "Bac+3");
    
    for(Candidature candidature : c)
    {
      out.println(candidature.getId());
      out.println(candidature.getNom());
      out.println(candidature.getPrenom());
    }
 
    out.println("Liste des offres d'emploi :");
    List<Offreemploi> offres = offreemploiDAO.findAll();
    
    for(Offreemploi offre : offres)
    {
      out.println(offre.getTitre()); //a completer
    }
    out.println();
    
    out.println("Liste des offres selon secteur d'activité et niveau de qualification");
    List <Offreemploi> o = offreemploiDAO.findOffreBySecteurActiviteAndNiveauQualification(5, "Bac+5");
    
    for(Offreemploi offre : o)
    {
      out.println(offre.getNiveauqualification().getIntitule());
      out.println(offre.getDescriptifmission());
      out.println(offre.getProfilrecherche());
      out.println(offre.getTitre());
      out.println(offre.getDatedepot());
    }
    out.println();
  }
  //-----------------------------------------------------------------------------
}
